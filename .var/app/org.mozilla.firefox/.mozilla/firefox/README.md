# Firefox config

## Vimium

Load settings: `Backup` > `Restore` > select `vimium.json`

## tridactyl

Load settings from:

```
source --url https://gitlab.com/yellowhat/dotfiles/-/raw/main/.var/app/org.mozilla.firefox/.mozilla/firefox/tridactylrc
```