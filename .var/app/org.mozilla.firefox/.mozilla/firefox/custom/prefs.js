// Do not remember login
user_pref("signon.rememberSignons", false);
// Do not suggest password
user_pref("signon.generation.enabled", false);
// Do not search for breached web sites
user_pref("signon.management.page.breach-alerts.enabled", false);
// Do not use Firefox Relay email to mask your email address
user_pref("signon.firefoxRelay.feature", "disabled");

// Do not remember credit cards
user_pref("extensions.formautofill.creditCards.enabled", false);

// Disable picture-in-picture
user_pref("media.videocontrols.picture-in-picture.video-toggle.enabled", false);

// Make bookmark toolbar hidden
user_pref("browser.toolbars.bookmarks.visibility", "never");

// Ask where to save files
user_pref("browser.download.useDownloadDir", false);

// New tab is blank
user_pref("browser.newtabpage.enabled", false);
user_pref("browser.startup.homepage", "chrome://browser/content/blanktab.html");

// Open previous windows and tabs
user_pref("browser.startup.page", 3);

// Disable Welcome Tab
user_pref("browser.aboutwelcome.enabled", false);

// Prefer dark appearance
user_pref("layout.css.prefers-color-scheme.content-override", 0);

// Disable shortcuts
user_pref("browser.newtabpage.activity-stream.feeds.topsites", false);

// Define cache directory (check via about:cache)
user_pref("browser.cache.disk.parent_directory", "/tmp");

// Enable Hardware acceleration
user_pref("layers.acceleration.force-enabled", true);
user_pref("media.hardware-video-decoding.enabled", true);
user_pref("media.hardware-video-decoding.force-enabled", true);

// Force enable VA-API
user_pref("media.ffmpeg.vaapi.enabled", true);
// Enable hardware VA-API decoding for WebRTC
user_pref("media.navigator.mediadatadecoder_vpx_enabled", true);

// Opt-out Mozilla studies
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("datareporting.healthreport.uploadEnabled", false);

// Disable connection to Mozilla services
user_pref("dom.push.enabled", false);

// Do not allow to disable add-ons by itself
user_pref("extensions.quarantinedDomains.enabled", false);

// Disable Sync
user_pref("identity.fxaccounts.enabled", false);

// Disable recommends addons
user_pref(
    "browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons",
    false,
);

// Disable recommends features
user_pref(
    "browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features",
    false,
);

// Disable privacy-preserving ad measurement
user_pref("dom.private-attribution.submission.enabled", false);

// Search suggestions
user_pref("browser.search.suggest.enabled", false);
user_pref("browser.search.suggest.enabled.private", true);
user_pref("browser.urlbar.showSearchSuggestionsFirst", false);

// If pan with middle button, sometimes it pastes
user_pref("middlemouse.paste", false);
