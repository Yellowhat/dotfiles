-- Neovim options config file ~/.config/nvim/lua/options.lua

-- Leader key
vim.g.mapleader = ","
vim.g.maplocalleader = ","

-- Use true colors
vim.opt.termguicolors = true

-- Yank to system clipboard
vim.opt.clipboard = "unnamedplus"

-- Do not send single character deletes to the clipboard
vim.keymap.set("n", "x", '"_x')

-- Use Unix as the standard file type
vim.opt.ffs = "unix"

-- Use utf-8 encoding
vim.opt.fileencoding = "utf-8"

-- How to show tabs, spaces, eol
vim.opt.list = true
vim.opt.listchars = {
    space = " ",
    tab = "▸ ",
    trail = "•",
    eol = " ",
}

-- Ignore compiled files
vim.opt.wildignore = { "*.o", "*~", "*.pyc" }

-- Show line number
vim.opt.number = true
vim.opt.relativenumber = false

-- Wrap left and right using arrows
vim.opt.whichwrap:append({
    ["<"] = true,
    [">"] = true,
    h = true,
    l = true,
    ["["] = true,
    ["]"] = true,
})

-- Completion
vim.opt.completeopt = { "menuone", "noselect" }

-- Highlight current line
vim.opt.cursorline = true

-- Number of lines to see above and below the cursor
vim.opt.scrolloff = 8
vim.opt.sidescrolloff = 8

-- Spelling
vim.api.nvim_set_keymap("n", "<F11>", ":set spell!<CR>", { noremap = true })
vim.opt.spelllang = { "en", "cjk" }

-- Backup/swap/undo files
vim.opt.backup = false
vim.opt.writebackup = false
vim.opt.swapfile = false
vim.opt.undofile = true
vim.opt.undodir = "/tmp"

-- Search/Replace
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Tab (1 tab = 4 spaces)
vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4
vim.opt.softtabstop = 4

-- Remove trailing spaces on save
vim.api.nvim_create_autocmd("BufWritePre", {
    pattern = "*",
    command = [[%s/\s\+$//e]],
})

-- Recognize helm files as Go template file instead of yaml
vim.api.nvim_create_autocmd({
    "BufNewFile",
    "BufRead",
}, {
    pattern = "*/templates/*.yaml,*/templates/*.tpl,*.gotmpl,helmfile*.yaml",
    callback = function()
        local buf = vim.api.nvim_get_current_buf()
        vim.api.nvim_set_option_value("filetype", "gotmpl", { buf = buf })
        vim.opt.tabstop = 2
        vim.opt.shiftwidth = 2
    end,
})

-- GO language
vim.api.nvim_create_autocmd({
    "BufNewFile",
    "BufRead",
}, {
    pattern = "*.go",
    callback = function()
        vim.opt.expandtab = false
    end,
})

-- Nix language
vim.api.nvim_create_autocmd({
    "BufNewFile",
    "BufRead",
}, {
    pattern = "*.nix",
    callback = function()
        vim.opt.tabstop = 2
        vim.opt.shiftwidth = 2
    end,
})

-- Template new bash script
vim.api.nvim_create_autocmd("BufNewFile", {
    pattern = "*.sh",
    command = "silent! 1s:.*:#!/usr/bin/env bash\r#\rset -euo pipefail\r",
})
