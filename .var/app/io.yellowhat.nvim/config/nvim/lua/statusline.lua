-- Neovim statusline config file ~/.config/nvim/lua/statusline.lua

function _G.statusline()
    return string.format(
        "%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
        [[%#DiffAdd#%{(mode()=='n')?'  NORMAL ':''}]],
        [[%#DiffChange#%{(mode()=='i')?'  INSERT ':''}]],
        [[%#DiffDelete#%{(mode()=='r')?'  REPLACE ':''}]],
        [[%#Cursor#%{(mode()=='v')?'  VISUAL ':''}]],
        " %n", -- Buffer number
        " %R", -- Readonly flag
        " %M", -- Modified [+] flag
        " %f", -- File path
        " %=", -- Align right section
        " %y ", -- File type
        [[%{&fileencoding?&fileencoding:&encoding}]], -- File encoding
        " [%{&fileformat}]", -- File format
        " %6l:%-3c", -- Line : Column
        " %3p%%" -- Percentage
    )
end

vim.opt.statusline = statusline()
