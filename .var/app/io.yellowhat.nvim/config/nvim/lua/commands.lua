-- Neovim commands config file ~/.config/nvim/lua/commands.lua

-- Order words in line
vim.api.nvim_create_user_command("OrderInLine", "s/\\v\\s+/\\r/g|'[,sort|,']j|nohl", {})
