-- Neovim theme config file ~/.config/nvim/lua/theme.lua

vim.cmd.colorscheme("vim")

vim.api.nvim_set_hl(0, "Pmenu", { bg = "#000000", fg = "#ffffff" })
