-- Neovim keymaps config file ~/.config/nvim/lua/keymaps.lua

local opts = {
    noremap = true, -- non-recursive mapping
    silent = true, -- execute silently
}

-- Replace all alias to "S"
vim.api.nvim_set_keymap("", "S", ":%s///g<Left><Left><Left>", { noremap = true })

-- Substitute all occurrences of the word under the cursor
vim.api.nvim_set_keymap("", "<leader>s", ":%s/<C-r><C-w>//g<Left><Left>", { noremap = true })

-- Save
vim.api.nvim_set_keymap("", "<C-s>", ":w<CR>", opts)
vim.api.nvim_set_keymap("i", "<C-s>", "<ESC>:w<CR>i", opts)

-- Go to Beginning of the line
vim.api.nvim_set_keymap("", "<C-a>", "<ESC>^", opts)
vim.api.nvim_set_keymap("i", "<C-a>", "<ESC>I", opts)

-- Go to End of the line
vim.api.nvim_set_keymap("", "<C-e>", "<ESC>$", opts)
vim.api.nvim_set_keymap("i", "<C-e>", "<ESC>A", opts)

-- Remove last word
vim.api.nvim_set_keymap("i", "<C-H>", "<C-W>", opts)

-- Turn off search highlighting
vim.api.nvim_set_keymap("", "<ESC><ESC>", ":nohlsearch<CR>", opts)

-- File Explorer
vim.api.nvim_set_keymap("", "<F2>", ":NERDTreeToggle<CR>", opts)

-- Autoindent file
vim.api.nvim_set_keymap("", "<F4>", "mzgg=G`z", opts)

-- Show hidden characters
vim.api.nvim_set_keymap("", "<F8>", ":set invlist<CR>", opts)

-- Switching between buffers
vim.api.nvim_set_keymap("", "<C-S-Tab>", ":bprevious<CR>", opts)
vim.api.nvim_set_keymap("", "<C-Tab>", ":bnext<CR>", opts)
vim.api.nvim_set_keymap("", "<C-h>", ":bprevious<CR>", opts)
vim.api.nvim_set_keymap("", "<C-l>", ":bnext<CR>", opts)

-- Stay in indent mode (visual mode only)
vim.api.nvim_set_keymap("v", "<", "<gv", opts)
vim.api.nvim_set_keymap("v", ">", ">gv", opts)

-- Move text up and down (visual mode only)
vim.api.nvim_set_keymap("x", "<C-j>", ":move '>+1<CR>gv-gv", opts)
vim.api.nvim_set_keymap("x", "<C-Down>", ":move '>+1<CR>gv-gv", opts)
vim.api.nvim_set_keymap("x", "<C-k>", ":move '<-2<CR>gv-gv", opts)
vim.api.nvim_set_keymap("x", "<C-Up>", ":move '<-2<CR>gv-gv", opts)

-- Formatter
vim.api.nvim_set_keymap("", "=f", ":lua vim.lsp.buf.formatting_sync()<CR>", opts)
vim.api.nvim_set_keymap("", "=j", ":%!python -m json.tool<CR>", opts)
