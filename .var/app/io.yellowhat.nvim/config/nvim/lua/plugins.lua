-- Neovim plugins config file ~/.config/nvim/lua/plugins.lua

-- Automatically install lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "--branch=stable", -- latest stable release
        "https://github.com/folke/lazy.nvim.git",
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

return require("lazy").setup({
    { -- File system explorer
        "preservim/nerdtree",
    },
    { -- Comment multiple lines using gc
        "numToStr/Comment.nvim",
        opts = {},
    },
    { -- Adds git related signs to the gutter
        "lewis6991/gitsigns.nvim",
        opts = {
            signs = {
                add = { text = "+" },
                change = { text = "~" },
                delete = { text = "_" },
                topdelete = { text = "‾" },
                changedelete = { text = "~" },
            },
            current_line_blame = true,
        },
    },
    { -- Show buffers as tabs
        "akinsho/bufferline.nvim",
        dependencies = {
            "kyazdani42/nvim-web-devicons",
        },
        config = function()
            require("bufferline").setup({
                options = {
                    numbers = function(opts)
                        return string.format("%s.", opts.ordinal)
                    end,
                    -- numbers = "ordinal",
                    -- number_style = "none",
                    separator_style = "thin",
                    always_show_bufferline = false,
                    show_buffer_icons = false,
                    show_buffer_close_icons = false,
                    show_close_icon = false,
                },
            })
        end,
    },
    { -- Adds indentation guides
        "lukas-reineke/indent-blankline.nvim",
        config = function()
            local highlight = {
                "IndentLevel1",
                "IndentLevel2",
                "IndentLevel3",
                "IndentLevel4",
                "IndentLevel5",
                "IndentLevel6",
            }
            local hooks = require("ibl.hooks")
            hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
                vim.api.nvim_set_hl(0, "IndentLevel1", { bg = "#2c1517", fg = "#E06C75" })
                vim.api.nvim_set_hl(0, "IndentLevel2", { bg = "#16130c", fg = "#E5C07B" })
                vim.api.nvim_set_hl(0, "IndentLevel3", { bg = "#0f130c", fg = "#98C379" })
                vim.api.nvim_set_hl(0, "IndentLevel4", { bg = "#081213", fg = "#56B6C2" })
                vim.api.nvim_set_hl(0, "IndentLevel5", { bg = "#091117", fg = "#61AFEF" })
                vim.api.nvim_set_hl(0, "IndentLevel6", { bg = "#130c16", fg = "#C678DD" })
            end)
            require("ibl").setup({
                indent = { highlight = highlight },
                whitespace = {
                    highlight = highlight,
                    remove_blankline_trail = false,
                },
            })
        end,
    },
    { -- Alignment plugin
        "junegunn/vim-easy-align",
        cmd = "EasyAlign",
    },
    { -- Highlight, edit, and navigate code
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        config = function()
            require("nvim-treesitter.configs").setup({
                ensure_installed = {
                    "bash",
                    "comment",
                    "dockerfile",
                    "fish",
                    "go",
                    "gotmpl",
                    "hcl",
                    "hjson",
                    "json",
                    "json5",
                    "lua",
                    "make",
                    "markdown",
                    "python",
                    "regex",
                    "rust",
                    "terraform",
                    "vim",
                    "yaml",
                },
                -- Autoinstall languages that are not installed
                auto_install = true,
                highlight = {
                    enable = true,
                },
                indent = {
                    enable = true,
                },
            })
        end,
    },
    {
        "williamboman/mason-lspconfig.nvim",
        dependencies = {
            "neovim/nvim-lspconfig",
            "williamboman/mason.nvim",
            "williamboman/mason-lspconfig.nvim",
            "WhoIsSethDaniel/mason-tool-installer.nvim",
        },
        config = function()
            -- LSP servers and clients are able to communicate to each other what features they support.
            --  By default, Neovim doesn't support everything that is in the LSP Specification.
            --  When you add nvim-cmp, luasnip, etc. Neovim now has *more* capabilities.
            --  So, we create new capabilities with nvim cmp, and then broadcast that to the servers.
            local capabilities = vim.lsp.protocol.make_client_capabilities()
            capabilities = vim.tbl_deep_extend("force", capabilities, require("cmp_nvim_lsp").default_capabilities())

            local servers = {
                -- HTML/CSS/XML
                cssls = {},
                emmet_ls = {},
                html = {},
                lemminx = {},
                -- GO
                golangci_lint_ls = {},
                gopls = {},
                -- Javascript/Typescript/JSON
                biome = {},
                eslint = {},
                jsonls = {},
                -- Lua
                lua_ls = {
                    settings = {
                        Lua = {
                            runtime = { version = "LuaJIT" },
                            workspace = {
                                checkThirdParty = false,
                                library = {
                                    "${3rd}/luv/library",
                                    unpack(vim.api.nvim_get_runtime_file("", true)),
                                },
                            },
                            completion = {
                                callSnippet = "Replace",
                            },
                        },
                    },
                },
                stylua = {},
                -- Markdown
                marksman = {},
                prosemd_lsp = {},
                zk = {},
                -- Python
                pyre = {},
                pylsp = {
                    settings = {
                        pylsp = {
                            plugins = {
                                flake8 = {
                                    maxLineLength = 119,
                                },
                                pycodestyle = {
                                    maxLineLength = 119,
                                },
                            },
                        },
                    },
                },
                pyright = {},
                ruff = {
                    init_options = {
                        settings = {
                            args = {
                                "--line-length=119",
                            },
                        },
                    },
                },
                -- Terraform
                terraformls = {},
                tflint = {},
                -- YAML
                yamlls = {
                    settings = {
                        yaml = {
                            format = {
                                enable = true,
                            },
                            schemaStore = {
                                url = "https://www.schemastore.org/api/json/catalog.json",
                                enable = true,
                            },
                            schemas = {
                                kubernetes = {
                                    "configmap*.y*ml",
                                    "crd*.y*ml",
                                    "cronjob*.y*ml",
                                    "daemonset*.y*ml",
                                    "deployment*.y*ml",
                                    "namespace*.y*ml",
                                    "pv*.y*ml",
                                    "pvc*.y*ml",
                                    "rbac*.y*ml",
                                    "secret*.y*ml",
                                    "service*.y*ml",
                                    "serviceaccount*.y*ml",
                                    "statefulset*.y*ml",
                                    "storageclass*.y*ml",
                                },
                            },
                            keyOrdering = false,
                        },
                    },
                },
                -- Other
                ansiblels = {}, -- ansible
                bashls = {}, -- bash
                dockerls = {}, -- dockerfile
                helm_ls = {}, -- helm
                typos_lsp = {}, -- spelling checker
                rust_analyzer = {}, -- rust
            }
            require("mason").setup()

            local ensure_installed = vim.tbl_keys(servers or {})
            vim.list_extend(ensure_installed, {})
            require("mason-tool-installer").setup({ ensure_installed = ensure_installed })

            require("mason-lspconfig").setup({
                handlers = {
                    function(server_name)
                        local server = servers[server_name] or {}
                        -- This handles overriding only values explicitly passed
                        -- by the server configuration above. Useful when disabling
                        -- certain features of an LSP
                        server.capabilities = vim.tbl_deep_extend("force", {}, capabilities, server.capabilities or {})
                        require("lspconfig")[server_name].setup(server)
                    end,
                },
            })
        end,
    },
    { -- Autocompletion plugin
        "hrsh7th/nvim-cmp",
        dependencies = {
            "hrsh7th/cmp-buffer", -- Buffer completions
            "hrsh7th/cmp-path", -- Path completions
            "hrsh7th/cmp-cmdline", -- Cmdline completions
            "saadparwaiz1/cmp_luasnip", -- Snippet completions
            "hrsh7th/cmp-nvim-lsp", -- Lsp completions
            "hrsh7th/cmp-nvim-lua",
            "L3MON4D3/LuaSnip", -- Snippet engine
            "rafamadriz/friendly-snippets", -- Multiple snippets to use
        },
        config = function()
            local cmp = require("cmp")
            cmp.setup({
                snippet = {
                    expand = function(args)
                        require("luasnip").lsp_expand(args.body)
                    end,
                },
                sources = {
                    { name = "nvim_lsp" },
                    { name = "luasnip" },
                    { name = "buffer" },
                    { name = "path" },
                },
                mapping = {
                    ["<Tab>"] = cmp.mapping.select_next_item(),
                    ["<S-Tab>"] = cmp.mapping.select_prev_item(),
                    ["<C-w>"] = cmp.mapping.close(),
                    ["<CR>"] = cmp.mapping.confirm({
                        behavior = cmp.ConfirmBehavior.Replace,
                        select = true,
                    }),
                },
                window = {
                    completion = cmp.config.window.bordered(),
                    documentation = cmp.config.window.bordered(),
                },
            })
        end,
    },
    { -- Highlight todo, notes, etc in comments
        "folke/todo-comments.nvim",
        event = "VimEnter",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
        opts = {
            signs = false,
        },
    },
})
