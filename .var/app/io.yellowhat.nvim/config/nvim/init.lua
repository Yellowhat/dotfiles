-- Neovim config file ~/.config/nvim/init.lua

require("options")
require("keymaps")
require("statusline")
require("theme")
require("plugins")
require("commands")
