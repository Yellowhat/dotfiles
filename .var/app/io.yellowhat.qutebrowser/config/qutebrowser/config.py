"""qutebrowser config file"""

# pylint: disable=E0401
import sys
from os.path import dirname, expanduser
from qutebrowser.qutebrowser import get_argparser

# pylint: disable=C0111
c = c  # noqa: F821 pylint: disable=E0602,C0103,W0127
config = config  # noqa: F821 pylint: disable=E0602,C0103,W0127

# Do not load customization via the UI
config.load_autoconfig(False)

# Parse Input ---------------------------------------------------------------------------------------------------------
args = get_argparser().parse_args(sys.argv[1:])
args = dict(args.temp_settings)

c.content.private_browsing = args.get("content.private_browsing", False) == "True"
# =====================================================================================================================

# General -------------------------------------------------------------------------------------------------------------
c.auto_save.session = True
c.content.dns_prefetch = True
c.content.fullscreen.window = True
c.content.prefers_reduced_motion = True
c.scrolling.smooth = True
c.hints.uppercase = True
c.statusbar.position = "top"
c.qt.workarounds.disable_hangouts_extension = True

# Javascript
# c.content.javascript.enabled = False
# config.set("content.javascript.enabled", True, "https://*.mailbox.org/*")
# config.set("content.javascript.enabled", True, "https://*.reader.miniflux.app/*")
# config.set("content.javascript.enabled", True, "https://*.gitlab.com/*")
# config.set("content.javascript.enabled", True, "https://*.gentoo.org/*")

# Fingerprinting
# c.content.headers.accept_language = "en-US,en;q=0.9"
# c.content.headers.custom = {"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"}
# c.content.headers.user_agent = "Mozilla/5.0 (Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.122"

# GPU
c.qt.args = [
    # "enable-features=Vulkan,VaapiVideoDecoder,UseOzonePlatform",
    # "ozone-platform=wayland",
    "disable-gpu-memory-buffer-video-frames",
    "enable-gpu-compositing",
    "ignore-gpu-blocklist",
    "enable-gpu-rasterization",
    "enable-oop-rasterization",
    "enable-webrtc-pipewire-capturer",
    "enable-zero-copy",
    "enable-native-gpu-memory-buffers",
    "enable-accelerated-mjpeg-decode",
    "enable-accelerated-video",
    "enable-accelerated-video-decode",
    "num-raster-threads=4",
]

# View pdf in the browser
c.content.pdfjs = True

# Download
c.downloads.location.directory = "~/Downloads"
c.downloads.position = "bottom"

# Editor
c.editor.command = ["vim", "-f", "{}"]

# Open a new tab in the existing window and activate the window
c.new_instance_open_target = "tab"
# Open new tabs in the most recently focused window
c.new_instance_open_target_window = "last-focused"

# Tab
c.tabs.title.format = "{current_title}{audio}{private}"
c.tabs.background = True

# Url
c.url.default_page = "https://searx.v4d4.org"

c.url.searchengines = {
    "DEFAULT": "https://searx.v4d4.org/search?q={}",
    "d": "https://duckduckgo.com/lite/?q={}&kae=t&kaq=-1&kp=-2&kaj=m&k7=000000&kj=222222",
    "g": "https://google.com/search?udm=14&q={}",
    "gi": "https://google.com/search?udm=14&tbm=isch&q={}",
    "m": "https://google.com/maps/search/{}/",
    "o": "https://openstreetmap.org/search?query={}",
    "p": "https://packages.gentoo.org/packages/search?q={}",
    "s": "https://searx.v4d4.org/search?q={}",
    "t": "https://bt4g.org/search/{}",
    "t1": "https://flixhq.to/search/{}",
    "y": "https://youtube.com/results?search_query={}",
    "pt": "https://search.joinpeertube.org/search?search={}",
    "z": "https://gpo.zugaina.org/Search?search={}",
}

if c.content.private_browsing:
    start_pages = [c.url.default_page]
else:
    start_pages = [
        "https://login.mailbox.org/en",
        "https://reader.miniflux.app",
        "https://gitlab.com/yellowhat-labs",
    ]
c.url.start_pages = start_pages

test_pages = [
    "https://browserleaks.com/ip",
    "https://coveryourtracks.eff.org",
    "https://mullvad.net/en/check",
    "https://deviceinfo.me",
    "https://dnssec.vs.uni-due.de",
    "http://checkmyip.torrentprivacy.com",
    "https://grc.com/shieldsup",
]

c.window.title_format = "{perc}{current_title}{title_sep}{private}"
# =====================================================================================================================

# BINDINGS ------------------------------------------------------------------------------------------------------------
c.bindings.commands = {
    "normal": {
        "<1>": "tab-prev",
        "<2>": "tab-next",
        "<t>": "fake-key T",
        "<m>": "fake-key M",
        "<alt+f>": "fake-key f",
        "<alt+v>": "fake-key v",
        "<shift-a>": "fake-key <Shift-A>",
        "<ctrl-c>": "yank selection",
        "<ctrl-f>": "set-cmd-text /",
        "<alt+Left>": "back",
        "<alt+Right>": "forward",
        "<alt+w>": ";; ".join(f"open -t {s}" for s in test_pages),
        "<\\>": """hint links spawn flatpak-spawn --host
            flatpak run io.mpv.Mpv
                --no-terminal
                --force-window
                --ytdl-format=bestvideo[height>=720]+bestaudio/best
                {hint-url}
        """.replace(
            "\n", ""
        ),
    },
}

config.bind(",a", "adblock-update")
config.bind(",h", "config-cycle -t -p content.blocking.enabled")
config.bind(",j", "config-cycle -t -p content.javascript.enabled")
# =====================================================================================================================

# Permission ----------------------------------------------------------------------------------------------------------
c.content.autoplay = True
c.content.canvas_reading = True
c.content.desktop_capture = False
c.content.geolocation = False
c.content.media.audio_capture = False
c.content.media.video_capture = False
c.content.media.audio_video_capture = False
c.content.notifications.enabled = False
c.content.persistent_storage = "ask"
c.content.register_protocol_handler = True
c.content.tls.certificate_errors = "ask"
c.content.webgl = True
# =====================================================================================================================

# Adblock -------------------------------------------------------------------------------------------------------------
if not c.content.private_browsing:
    c.content.blocking.enabled = True
    # Use cached hostlist
    adlist_file = expanduser("~/.var/app/io.yellowhat.qutebrowser/cache/ad.list")
    with open(adlist_file, encoding="UTF8") as f:
        c.content.blocking.hosts.lists = [ln for ln in f.read().splitlines() if ln]
    # Whitelist
    c.content.blocking.whitelist = [
        "www.duckduckgo.com",
        "duckduckgo.com",
        "www.gitlab.com",
        "gitlab.com",
    ]
# =====================================================================================================================

# Load theme ----------------------------------------------------------------------------------------------------------
config.source(dirname(__file__) + "/theme.py")
# =====================================================================================================================
