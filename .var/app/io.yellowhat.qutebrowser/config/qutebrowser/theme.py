"""
qutebrowser theme config file

Based on https://github.com/evannagle/qutebrowser-dracula-theme (Aug 5, 2018)
"""

# pylint: disable=C0111
c = c  # noqa: F821 pylint: disable=E0602,C0103,W0127
config = config  # noqa: F821 pylint: disable=E0602,C0103,W0127

# GENERAL -------------------------------------------------------------------------------------------------------------
FONT = "10pt Comic Mono"

palette = {
    "background": "#000000",
    "background-alt": "#282a36",
    "foreground": "#ffffff",
    "foreground-alt": "#f8f8f2",
    "border": "#282a36",
    "current-line": "#44475a",
    "selection": "#44475a",
    "comment": "#6272a4",
    "cyan": "#8be9fd",
    "green": "#50fa7b",
    "orange": "#ffb86c",
    "pink": "#ff79c6",
    "purple": "#bd93f9",
    "red": "#ff5555",
    "yellow": "#f7ca88",
}

standard_padding = {
    "top": 0,
    "right": 0,
    "bottom": 0,
    "left": 0,
}
# =====================================================================================================================

# Completion widget ---------------------------------------------------------------------------------------------------
# Text
c.colors.completion.fg = palette["foreground"]
# Background
c.colors.completion.even.bg = palette["background"]
c.colors.completion.odd.bg = palette["background-alt"]
# SELECTED - Background
c.colors.completion.item.selected.bg = palette["selection"]
# SELECTED - Text
c.colors.completion.item.selected.fg = palette["foreground"]
# MATCHED - Text
c.colors.completion.match.fg = palette["orange"]
# SELECTED - Bottom border
c.colors.completion.item.selected.border.bottom = palette["selection"]
# SCROLLBAR - Background
c.colors.completion.scrollbar.bg = palette["background"]
# SCROLLBAR - Test
c.colors.completion.scrollbar.fg = palette["foreground"]
# =====================================================================================================================

# Completion widget category -----------------------------------------------------------------------------------------
# Background
c.colors.completion.category.bg = palette["background"]
# Foreground
c.colors.completion.category.fg = palette["foreground"]
# Bottom border
c.colors.completion.category.border.bottom = palette["border"]
# Top border
c.colors.completion.category.border.top = palette["border"]
# Top border - Selected
c.colors.completion.item.selected.border.top = palette["selection"]
# =====================================================================================================================

# Download ------------------------------------------------------------------------------------------------------------
# BAR - Background
c.colors.downloads.bar.bg = palette["background"]
# ERRORS - Background
c.colors.downloads.error.bg = palette["background"]
# ERRORS - TEXT
c.colors.downloads.error.fg = palette["red"]
# STOP - Background
c.colors.downloads.stop.bg = palette["background"]
# Color gradient interpolation
#   - "rgb": Interpolate in the RGB color system.
#   - "hsv": Interpolate in the HSV color system.
#   - "hsl": Interpolate in the HSL color system.
#   - "none": Don"t show a gradient.
c.colors.downloads.system.bg = "rgb"
# =====================================================================================================================

# Hints ---------------------------------------------------------------------------------------------------------------
# Background
c.colors.hints.bg = palette["yellow"]
# Foregorund
c.colors.hints.fg = palette["background"]
# Border
c.hints.border = "2px solid " + palette["background-alt"]
# MATCHED - Text
c.colors.hints.match.fg = palette["foreground-alt"]
# =====================================================================================================================

# Keyhint -------------------------------------------------------------------------------------------------------------
# Background
c.colors.keyhint.bg = palette["background"]
# Text
c.colors.keyhint.fg = palette["purple"]
# Highlight color for keys to complete the current keychain
c.colors.keyhint.suffix.fg = palette["selection"]
# =====================================================================================================================

# Message -------------------------------------------------------------------------------------------------------------
# ERROR - Background
c.colors.messages.error.bg = palette["background"]
# ERROR - Border
c.colors.messages.error.border = palette["background-alt"]
# ERROR - Text
c.colors.messages.error.fg = palette["red"]

# INFO - Background
c.colors.messages.info.bg = palette["background"]
# INFO - Border
c.colors.messages.info.border = palette["background-alt"]
# INFO - Text
c.colors.messages.info.fg = palette["comment"]

# WARNING - Background
c.colors.messages.warning.bg = palette["background"]
# WARNING - Border
c.colors.messages.warning.border = palette["background-alt"]
# WARNING - Text
c.colors.messages.warning.fg = palette["red"]
# =====================================================================================================================

# Prompts -------------------------------------------------------------------------------------------------------------
# Background
c.colors.prompts.bg = palette["background"]
# Border
c.colors.prompts.border = "1px solid " + palette["background-alt"]
# Text
c.colors.prompts.fg = palette["cyan"]
# SELECTED - Background
c.colors.prompts.selected.bg = palette["selection"]
# =====================================================================================================================

# Statusbar -----------------------------------------------------------------------------------------------------------
# CARET - Background
c.colors.statusbar.caret.bg = palette["background"]
# CARET - Text
c.colors.statusbar.caret.fg = palette["orange"]
# CARET - Selection - Background
c.colors.statusbar.caret.selection.bg = palette["background"]
# CARET - Selection - Text
c.colors.statusbar.caret.selection.fg = palette["orange"]

# COMMAND - Background
c.colors.statusbar.command.bg = palette["background"]
# COMMAND - Text
c.colors.statusbar.command.fg = palette["pink"]

# COMMAND - Private - Background
c.colors.statusbar.command.private.bg = palette["background"]
# COMMAND - Private - Text
c.colors.statusbar.command.private.fg = palette["foreground-alt"]

# INSERT - Background
c.colors.statusbar.insert.bg = palette["background"]
# INSERT - Text
c.colors.statusbar.insert.fg = palette["cyan"]

# NORMAL - Background
c.colors.statusbar.normal.bg = palette["background"]
# NORMAL - Text
c.colors.statusbar.normal.fg = palette["foreground"]

# PASSTHROUGH - Background
c.colors.statusbar.passthrough.bg = palette["background"]
# PASSTHROUGH - Text
c.colors.statusbar.passthrough.fg = palette["orange"]

# PRIVATE - Background
c.colors.statusbar.private.bg = palette["background-alt"]
# PRIVATE - Text
c.colors.statusbar.private.fg = palette["foreground-alt"]

# PROGRESS BAR - Background
c.colors.statusbar.progress.bg = palette["background"]
# PROGRESS BAR - Text
c.colors.statusbar.url.fg = palette["foreground"]
# PROGRESS BAR - Error - Text
c.colors.statusbar.url.error.fg = palette["red"]
# PROGRESS BAR - Hover - Text
c.colors.statusbar.url.hover.fg = palette["cyan"]
# PROGRESS BAR - Success - Text [http]
c.colors.statusbar.url.success.http.fg = palette["green"]
# PROGRESS BAR - Success - Text [https]
c.colors.statusbar.url.success.https.fg = palette["green"]
# PROGRESS BAR - Warning - Text
c.colors.statusbar.url.warn.fg = palette["yellow"]

# PADDING
c.statusbar.padding = standard_padding
# =====================================================================================================================

# Tabs ----------------------------------------------------------------------------------------------------------------
# Background
c.colors.tabs.bar.bg = palette["background"]

# SELECTED - Background
c.colors.tabs.selected.even.bg = palette["background"]
c.colors.tabs.selected.odd.bg = palette["background"]
# SELECTED - Text
c.colors.tabs.selected.even.fg = palette["foreground"]
c.colors.tabs.selected.odd.fg = palette["foreground"]

# UNSELECTED - Background
c.colors.tabs.even.bg = palette["selection"]
c.colors.tabs.odd.bg = palette["selection"]
# UNSELECTED - Text
c.colors.tabs.even.fg = palette["foreground-alt"]
c.colors.tabs.odd.fg = palette["foreground-alt"]

# INDICATOR - Error
c.colors.tabs.indicator.error = palette["red"]
# INDICATOR - Start
c.colors.tabs.indicator.start = palette["orange"]
# INDICATOR - Stop
c.colors.tabs.indicator.stop = palette["green"]

# Color gradient interpolation
#   - "rgb": Interpolate in the RGB color system.
#   - "hsv": Interpolate in the HSV color system.
#   - "hsl": Interpolate in the HSL color system.
#   - "none": Don"t show a gradient.
c.colors.tabs.indicator.system = "rgb"

# PADDING
c.tabs.padding = standard_padding
c.tabs.indicator.width = 5
c.tabs.favicons.scale = 1.3
# =====================================================================================================================

# Fonts ---------------------------------------------------------------------------------------------------------------
c.fonts.default_family = ["Liberation Sans", "JoyPixels", "Noto Color Emoji"]
c.fonts.completion.entry = FONT
c.fonts.completion.category = "bold"
c.fonts.debug_console = FONT
c.fonts.downloads = FONT
c.fonts.hints = FONT
c.fonts.keyhint = FONT
c.fonts.messages.error = FONT
c.fonts.messages.info = FONT
c.fonts.messages.warning = FONT
c.fonts.prompts = FONT
c.fonts.statusbar = FONT
c.fonts.tabs.selected = FONT
c.fonts.tabs.unselected = FONT
c.fonts.web.family.standard = ""
c.fonts.web.family.fixed = ""
c.fonts.web.family.serif = ""
c.fonts.web.family.sans_serif = ""
c.fonts.web.family.cursive = ""
c.fonts.web.family.fantasy = ""
c.fonts.web.size.default = 16
c.fonts.web.size.default_fixed = 13
c.fonts.web.size.minimum = 0
c.fonts.web.size.minimum_logical = 6
# =====================================================================================================================

# Dark colors for websites --------------------------------------------------------------------------------------------
# c.colors.webpage.darkmode.enabled = True
# c.colors.webpage.darkmode.threshold.text = 150
# c.colors.webpage.darkmode.threshold.background = 205
# =====================================================================================================================
