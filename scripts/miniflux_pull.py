#!/usr/bin/env python
"""Pull starred entries from Miniflux"""

from miniflux import Client
from yt_dlp import YoutubeDL

username = input("Username: ")
password = input("Password: ")

client = Client("https://reader.miniflux.app", username, password)
feeds = client.get_feeds()
entries = client.get_entries(starred=True, limit=0)
urls = [e["url"] for e in entries["entries"]]
print(f"[INFO] # URLs: {len(urls)}")

ydl_opts = {
    "ignoreerrors": True,
    "format": "best",
    "writesubtitles": True,
    "external_downloader": "aria2c",
    "external_downloader_args": [
        "--continue",
        "--max-concurrent-downloads",
        "16",
        "--max-connection-per-server",
        "16",
        "--split",
        "16",
        "--min-split-size",
        "1M",
    ],
}

with YoutubeDL(ydl_opts) as ydl:
    ydl.download(urls)
