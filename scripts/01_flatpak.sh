#!/usr/bin/env bash
# Install Flatpaks
set -euo pipefail

# Repository
flatpak --user remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak --user remote-add --if-not-exists flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
flatpak --user remote-add --if-not-exists yellowhat https://yellowhat-labs.gitlab.io/flatpak/flathub.flatpakrepo

# Application
args=(
    org.mozilla.firefox
    io.mpv.Mpv
    org.qbittorrent.qBittorrent
    org.libreoffice.LibreOffice
    org.remmina.Remmina
    com.github.xournalpp.xournalpp
    org.freedesktop.Sdk.Extension.golang//24.08
    org.freedesktop.Sdk.Extension.rust-stable//24.08
    org.freedesktop.Sdk.Extension.node22//24.08
    # Required to use video acceleration in firefox
    org.freedesktop.Platform.ffmpeg-full/x86_64/23.08
)
flatpak --user install -y flathub "${args[@]}"

flatpak --user install -y flathub-beta \
    org.gimp.GIMP

flatpak --user install -y yellowhat \
    io.yellowhat.nvim \
    io.yellowhat.qutebrowser \
    io.yellowhat.librewolf \
    io.yellowhat.zathura

# Override
args=(
    --filesystem host
    # By default GO create a go folder in HOME
    --env GOPATH="${HOME}/.var/app/io.yellowhat.nvim/go"
    # Mount rust, go, ... flapak extensions
    --env FLATPAK_ENABLE_SDK_EXT="*"
)
flatpak --user override "${args[@]}" io.yellowhat.nvim
