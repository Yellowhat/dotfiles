#!/usr/bin/env bash
set -euo pipefail

podman run \
    --interactive \
    --tty \
    --rm \
    --volume "$PWD":/data \
    --workdir /data \
    archlinux \
    /bin/bash -c "
            pacman -Syu --noconfirm aria2 ffmpeg python-pip \
         && pip install miniflux yt-dlp \
         && python miniflux_pull.py"
