#!/usr/bin/env bash
# Update repository files from local
set -euo pipefail

files=$(find .config .local etc usr -type f)

for file in $files; do
    echo "$file"
    if [[ "$file" == .* ]]; then
        prefix=~/
    else
        prefix=/
    fi
    cp "$prefix$file" "$file"
done
