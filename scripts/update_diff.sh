#!/usr/bin/env bash
# Update local files from repository
set -euo pipefail

COLUMNS=$(tput cols)

files=$(find .config .local .var etc usr -type f)
for file in $files; do
    if [[ "$file" == .* ]]; then
        prefix=~/
        user=$(whoami)
    else
        prefix=/
        user=root
    fi
    if ! sudo -u "$user" diff --unified --color=auto "${prefix}${file}" "$file" >/dev/null 2>&1; then
        echo "Differ: $file"
    fi
done
