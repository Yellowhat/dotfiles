#!/usr/bin/env bash
# Update local files from repository
set -euo pipefail

files=$(find .config .local .var etc usr -type f)

for file in $files; do
    echo "$file"
    if [[ "$file" == .* ]]; then
        prefix=~/
        user=$(whoami)
    else
        prefix=/
        user=root
    fi
    sudo -u "$user" mkdir -p "$prefix$(dirname "$file")"
    sudo -u "$user" cp "$file" "$prefix$file"
done
