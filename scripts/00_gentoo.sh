#!/usr/bin/env bash
# Gentoo Installation handbook
set -euo pipefail

# Partition
mkfs.xfs /dev/sda1
mkdir /mnt/gentoo
mount /dev/sda1 /mnt/gentoo
mkdir /mnt/gentoo/tmp
mkdir /mnt/gentoo/var/tmp
mount -t tmpfs -o size=12G tmpfs /mnt/gentoo/tmp/
mount -t tmpfs -o size=12G tmpfs /mnt/gentoo/var/tmp/

# Stage
cd /mnt/gentoo
wget http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64-nomultilib/stage3-amd64-nomultilib-*.tar.xz
tar xf stage3-*.tar.xz

# Chroot
cp -L /etc/resolv.conf /mnt/gentoo/etc/
mount -t proc /proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --make-rslave /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
mount --make-rslave /mnt/gentoo/dev
test -L /dev/shm && rm /dev/shm && mkdir /dev/shm
mount -t tmpfs -o nosuid,nodev,noexec shm /dev/shm
chmod 1777 /dev/shm

chroot /mnt/gentoo /bin/bash
env-update
emerge-webrsync
emerge --sync --quiet

# System config files
# - /etc/fstab
# - /etc/portage/make.conf
# - /etc/portage/package.*

# Select profile
eselect profile list
eselect profile set default/linux/amd64/23.0/no-multilib/systemd

# Locale
echo "en_GB.UTF-8 UTF-8" >/etc/locale.gen
locale-gen
eselect locale list
eselect locale set 4

# Time zone
cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime

# Portage
emerge -1a portage

# Update gcc
emerge -1a gcc
gcc-config "$(gcc-config -l | tail -1 | awk '{print $2}')"
env-update

# Remerge world
emerge -avuDN --jobs 2 @world
emerge -av --depclean

# Kernel
emerge -a sys-kernel/gentoo-kernel-bin
# emerge -a sys-kernel/vanilla-sources
# eselect kernel set linux-5.x.y
# cd /usr/src/linux
# make menuconfig
# make -j5 && make modules_install
# cp arch/x86_64/boot/bzImage /boot/kernel-5.x.y

# Base Software
emerge -a fish neovim fzf power top btop igp-gpu-tools iotop app-text/tree sudo
emerge -a comic-mono joypixels nanum noto
emerge -a networkmanager wireguard-tools
emerge -a eix portage-utils gentoolkit genlop
emerge -a ntfs3g dosfstools xfsprogs lm-sensors linux-firmware

# Users
useradd -m -G audio,input,kvm,usb,video,wheel --shell "$(command -v fish)" vasco
passwd vasco
passwd

cat >/etc/sudoers.d/vasco <<EOF
vasco ALL=(ALL) NOPASSWD: ALL
vasco ALL=NOPASSWD: /sbin/halt, /sbin/reboot, /sbin/poweroff
EOF

# Grub
emerge -av grub
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg
# Modify `/etc/default/grub` and uncomment
# GRUB_CMDLINE_LINUX="init=/usr/lib/systemd/systemd mitigations=off"

# Reboot

# Network
systemctl enable --now NetworkManager
nmcli dev wifi rescan
nmcli dev wifi list
nmcli dev wifi connect "<NAME>" password "<PASSWORD>"
## DNS
emerge -a dnscrypt-proxy
systemctl disable --now systemd-resolved
systemctl enable --now dnscrypt-proxy.service

cat <<EOF >/etc/resolv.conf
nameserver ::1
nameserver 127.0.0.1
options edns0 single-request-reopen
EOF
# chattr +i /etc/resolv.conf
## Hostname
hostnamectl set-hostname janis
localectl set-locale LANG="en_GB.UTF8"
localectl set-keymap uk

# Application
emerge -a dev-vcs/git-lfs
emerge -a playerctl bluez simple-mtpfs unrar zip tcpdump
emerge -a libav-intel-driver
emerge -a sway swaybg grim slurp foot statusbar warpd wl-clipboard
emerge -a xdg-desktop-portal xdg-desktop-portal-wlr xdg-desktop-portal-gtk
emerge -a podman perf strace

# Pipewire
systemctl --user disable pulseaudio.socket
systemctl --user enable --now \
    pipewire.socket \
    pipewire-pulse.socket \
    wireplumber.service

# Flatpak (double check?)
emerge -a flatpak
sudo mkdir -p /var/lib/flatpak
sudo chmod -R "ugo=rwX" /var/lib/flatpak

# Wireguard
## Download config file from mullvad.net/en/account#/wireguard-config
cp mlvd-nl4.conf /etc/wireguard/
chown root:root -R /etc/wireguard
chmod 600 -R /etc/wireguard
## To connect
wg-quick up mlvd-nl4

# TRIM
cat >/etc/systemd/system/fstrim.service <<EOF
[Unit]
Description=Run fstrim on all mounted devices that support discard

[Service]
Type=oneshot
ExecStart=/bin/sh -c "/sbin/fstrim --all"
EOF
cat >/etc/systemd/system/fstrim.timer <<EOF
[Unit]
Description=Run fstrim.service weekly

[Timer]
OnCalendar=weekly
Persistent=true

[Install]
WantedBy=multi-user.target
EOF
chmod 644 /etc/systemd/system/fstrim.*
systemctl enable --now fstrim.timer
# It is now possible to see if it has been run and when the next time it will be ran by issuing:
systemctl list-timers

# Autologin
# mkdir -p /etc/systemd/system/getty@tty1.service.d
cat >/etc/systemd/system/getty@tty1.service.d/override.conf <<EOF
[Service]
ExecStart=
ExecStart=-/sbin/agetty --autologin vasco --noclear %I \$TER[Service]
EOF
