#!/usr/bin/env bash
# Fedora Installation handbook
set -euo pipefail

# Base system
# Install the Fedora Server Netinstall:
# - Base Environment: `Minimal Install`
# - Root: disable
# - Additional software for Selected Environment: empty
# - Partition:
#    - /boot/efi: EFI System Partition, 600 MiB
#    - /boot: xfs, 1024 MiB
#    - /: xfs, rest

# DNF
echo "install_weak_deps=False" | sudo tee -a /etc/dnf/dnf.conf
echo "max_parallel_downloads=10" | sudo tee -a /etc/dnf/dnf.conf

# General
sudo hostnamectl set-hostname sharko

sudo dnf copr enable -y yellowhat/yellowhat
sudo dnf install -y "https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm"
sudo dnf install -y "https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm"
sudo dnf update -y
sudo dnf install -y yellowhat
sudo dnf swap -y nano-default-editor vim-default-editor

sudo dnf remove -y zram-generator-defaults

sudo systemctl disable bluetooth
sudo systemctl disable sshd
systemctl --user enable --now \
    pipewire-pulse.service \
    wireplumber.service

sudo bash -c "cat >/etc/sudoers.d/$(whoami)" <<EOF
$(whoami) ALL=(ALL) NOPASSWD: ALL
$(whoami) ALL=NOPASSWD: /sbin/halt, /sbin/reboot, /sbin/poweroff
EOF

sudo usermod --shell "$(command -v fish)" "$(whoami)"

# Disable Nvidia GPU on boot
# sudo tee /etc/systemd/system/nvidiagpu-poweroff.service <<EOF
# [Unit]
# Description=Power-off Nvidia GPU
#
# [Service]
# Type=oneshot
# ExecStart=/bin/bash -c "echo -n auto >/sys/bus/pci/devices/0000:01:00.0/power/control"
#
# [Install]
# WantedBy=default.target
# EOF
# systemctl enable nvidiagpu-poweroff

# Disable mitigations
sudo grubby --update-kernel=ALL --args="mitigations=off"
# i915
# - Load HuC firmware for more functionality
# - Enable frame buffer compression for power savings
# sudo grubby --update-kernel=ALL --args="i915.enable_guc=2 i915.enable_fbc=1"
# Nouveau
# - Disable driver
# - Disable power management
# sudo grubby --update-kernel=ALL --args="nouveau.blacklist=1 nouveau.modeset=0 nouveau.runpm=0"
# Show
sudo grubby --info=ALL
# Reboot

# Check with
# sudo grep . /sys/module/i915/parameters/*
# sudo grep . /sys/module/nouveau/parameters/*

# Battery charge threshold
echo 50 | sudo tee /sys/class/power_supply/BAT0/charge_control_start_threshold
echo 65 | sudo tee /sys/class/power_supply/BAT0/charge_control_end_threshold

# Fonts
# sudo dnf install -y \
#     google-noto-emoji-color-fonts \
#     google-noto-sans-symbols-fonts \
#     google-noto-sans-symbols2-fonts

# sudo mkdir -p /usr/share/fonts/joypixels
# cd /usr/share/fonts/joypixels
# sudo curl -LO https://cdn.joypixels.com/arch-linux/font/6.6.0/joypixels-android.ttf
# cd /etc/fonts/conf.d
# sudo curl -LO https://gitlab.com/Yellowhat/ebuilds/-/raw/master/media-fonts/joypixels/files/75-joypixels.conf

# sudo mkdir -p /usr/share/fonts/comic-mono
# cd /usr/share/fonts/comic-mono
# sudo curl -LO https://dtinth.github.io/comic-mono-font/ComicMono.ttf
