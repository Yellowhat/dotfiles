#!/usr/bin/env bash
# Pick an emoji and copy to clipboard
set -euo pipefail

FILE=~/.cache/emoji
if [ ! -f "$FILE" ]; then
    # Custom symbols
    {
        echo " ← left arrow"
        echo " → right arrow"
        echo " € euro"
        echo " £ pound"
        echo " ä a umlaut"
        echo " ö o umlaut"
        echo " ü u umlaut"
        echo " ß"
    } >"$FILE"

    # Official emoji
    ver=$(
        curl -sL https://unicode.org/Public/emoji/ |
            grep -o -E 'href="[[:digit:]]*.[[:digit:]]' |
            tr -d 'href="' |
            tail -1
    )
    curl -sL "https://unicode.org/Public/emoji/${ver}/emoji-test.txt" |
        grep -v -e "^#" -e "^$" |
        awk -F "[;#]" '{print $3}' >>"$FILE"
fi

$MENU --header '"Pick an emoji:"' <"$FILE" | awk '{print $1}' | wl-copy
