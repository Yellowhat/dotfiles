#!/usr/bin/env bash
# Manjaro
set -euo pipefail

# Setup
# ./qemu_manjaro.sh -boot d -cdrom ~/manjaro-kde-20.0.3-minimal-200606-linux56.iso

BOOT_OPTS="."
VM_IMG="$HOME/.local/Manjaro.img"
if [ ! -f "$VM_IMG" ]; then
    qemu-img create -f raw -o preallocation=full "${VM_IMG}" 50G
    BOOT_OPTS="-boot d -cdrom $HOME/manjaro-kde-20.0.3-minimal-200606-linux56.iso"
fi
echo "Using VM image: $VM_IMG"
echo "Additional boot options: $BOOT_OPTS"

printf -v MAC_ADDR "52:54:%02x:%02x:%02x:%02x" $((RANDOM & 0xff)) $((RANDOM & 0xff)) $((RANDOM & 0xff)) $((RANDOM & 0xff))
echo "New MAC ADDRESS: $MAC_ADDR"

qemu-system-x86_64 \
    -name "Manjaro VM" \
    -smp 2 \
    -m 8G \
    -cpu host \
    -drive file="$VM_IMG",format=raw \
    -device e1000,netdev=net0,mac="$MAC_ADDR" \
    -netdev user,id=net0,hostfwd=tcp::10022-:22 \
    -enable-kvm \
    -k en-gb \
    -monitor stdio
#    "$BOOT_OPTS" \
#    "$@"
