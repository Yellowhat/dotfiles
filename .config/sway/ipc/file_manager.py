#!/usr/bin/env python3
"""Open multiple file manager windows"""

from json import loads
from os import environ
from socket import socket, AF_UNIX, SOCK_STREAM
from struct import calcsize, pack, unpack


WORKSPACE = "file"
TERM = environ.get("TERMINAL", "foot")
FILE = environ.get("FILE", "sf")
FM = f"{TERM} {FILE}"


def run(cmd="", msg_type=0):
    """Run command in sway"""

    key = b"i3-ipc"
    header_fmt = f"={len(key)}sII"
    header_size = calcsize(header_fmt)
    payload = cmd.encode("UTF-8")

    conn = socket(AF_UNIX, SOCK_STREAM)
    conn.connect(environ.get("SWAYSOCK"))
    conn.sendall(pack(header_fmt, key, len(payload), msg_type))
    conn.sendall(payload)
    header = conn.recv(header_size)
    key_return, msg_len, msg_type = unpack(header_fmt, header)
    assert key_return == key, "Key string didn't match"
    if msg_len > 0:
        data = bytearray(msg_len)
        buf = memoryview(data)
        pos = 0
        while pos < msg_len:
            pos += conn.recv_into(buf)
    conn.close()

    return loads(data)


def get_workspace(workspace):
    """Get workspace information"""

    tree = run("", 4)
    for lvl1 in tree["nodes"]:
        for lvl2 in lvl1["nodes"]:
            if lvl2["name"] == workspace:
                return lvl2
    raise ValueError(f"Workspace '{workspace}' not found")


def get_windows(workspace):
    """Get windows ids in `workspace`"""

    ids = []
    for lvl1 in get_workspace(workspace)["nodes"]:
        if "nodes" in lvl1 and lvl1["nodes"]:
            for lvl2 in lvl1["nodes"]:
                ids.append(lvl2["id"])
        else:
            ids.append(lvl1["id"])
    return ids


def get_window_id(workspace):
    """Get max window id on workspace"""

    n_ids = len(get_windows(workspace))
    while n_ids == len(get_windows(workspace)):
        pass
    ids = get_windows(workspace)
    return max(ids)


run(f"workspace {WORKSPACE}")

run(f"exec {FM} ~/Downloads")
con_id1 = get_window_id(WORKSPACE)

run(f"exec {FM} /media/Backup")
con_id2 = get_window_id(WORKSPACE)

run(f"[con_id={con_id1}] focus; splitv; exec {FM} ~/git")
con_id3 = get_window_id(WORKSPACE)

run(f"[con_id={con_id2}] focus; splitv; exec {FM} /mnt")
con_id4 = get_window_id(WORKSPACE)

run(f"[con_id={con_id1}] focus")
