# sway config file ~/.config/sway/key_binding

# Mod key (Mod1 == Alt, Mod4 == Meta)
set $mod Mod4

# Home row direction keys, like vim
set $left  h
set $right l
set $down  j
set $up    k

# Drag floating windows by holding down $mod and left mouse button.
# Resize them with right mouse button + $mod.
floating_modifier $mod normal

# Applications
bindsym $mod+return exec "$TERMINAL"
bindsym $mod+t exec "$TERMINAL_FLOATING"
bindsym $mod+F1 exec "$BROWSER_PRIVATE"
bindsym $mod+F2 exec "$BROWSER"
bindsym $mod+F3 exec "$BROWSER_2_PRIVATE"
bindsym $mod+F4 exec "$BROWSER_2"
bindsym $mod+F5 exec '$TERMINAL_FLOATING ~/.local/bin/snippets'
bindsym $mod+e exec "~/.config/sway/ipc/file_manager.py"
bindsym $mod+c exec wl-copy <"$($HOME/.local/bin/clipit -l | $MENU \"--preview 'cat {}'\")"
bindsym $mod+d      exec "$MENU --launcher"
bindsym $mod+Escape exec "$MENU --launcher"
bindcode $mod+49 exec '$TERMINAL_FLOATING python -ic "from math import *"'
bindsym Alt+Shift+s exec "slurp -d | grim -g - - | wl-copy -t image/png && wl-paste >~/$(date +'screenshot_%Y-%m-%d_%H%M%S.png')"
bindsym Alt+Shift+h exec '$TERMINAL_FLOATING ~/.local/bin/cheat_sheet'

# Kill focused window
bindsym Alt+F4 kill
bindsym $mod+q kill

# Kill focused workspace
bindsym $mod+Shift+q [workspace=__focused__] kill

# Exit sway
bindsym Alt+Ctrl+Shift+c     reload
bindsym Alt+Ctrl+Shift+r     restart
bindsym Alt+Ctrl+Shift+e     exec swaynag -t warning -m "Do you want to exit?" -b "Yes" "swaymsg exit"
bindsym Alt+Ctrl+Shift+s     exec killall swaynag
bindsym Alt+Ctrl+Shift+Next  exec "sudo poweroff"
bindsym Alt+Ctrl+Shift+Prior exec "sudo reboot"

# Brightness buttons
bindsym XF86MonBrightnessDown exec "~/.config/sway/scripts/set_brightness.sh -10 && pkill -USR1 -f statusbar"
bindsym F9                    exec "~/.config/sway/scripts/set_brightness.sh -10 && pkill -USR1 -f statusbar"
bindsym XF86MonBrightnessUp   exec "~/.config/sway/scripts/set_brightness.sh  10 && pkill -USR1 -f statusbar"
bindsym F10                   exec "~/.config/sway/scripts/set_brightness.sh  10 && pkill -USR1 -f statusbar"
bindsym Shift+F9              exec "~/.config/sway/scripts/set_brightness.sh -100"
bindsym Shift+F10             exec "~/.config/sway/scripts/set_brightness.sh  100 && pkill -USR1 -f statusbar"

# Volume buttons
bindsym XF86AudioLowerVolume exec "wpctl set-volume @DEFAULT_AUDIO_SINK@ 10%- && pkill -USR1 -f statusbar"
bindsym F7                   exec "wpctl set-volume @DEFAULT_AUDIO_SINK@ 10%- && pkill -USR1 -f statusbar"
bindsym XF86AudioRaiseVolume exec "wpctl set-volume @DEFAULT_AUDIO_SINK@ 10%+ && pkill -USR1 -f statusbar"
bindsym F8                   exec "wpctl set-volume @DEFAULT_AUDIO_SINK@ 10%+ && pkill -USR1 -f statusbar"
bindsym XF86AudioMicMute     exec "wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle"
bindsym XF86AudioMute        exec "wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle && pkill -USR1 -f statusbar"

# Media player controls
bindsym XF86AudioPlay  exec "playerctl play-pause"
bindsym XF86AudioPause exec "playerctl play-pause"
bindsym XF86AudioNext  exec "playerctl next"
bindsym XF86AudioPrev  exec "playerctl previous"

# Reopen current focused window split (34: [, 35: ], 51: #)
bindcode $mod+Shift+34 exec "~/.config/sway/scripts/new_split_window.sh vertical"
bindcode $mod+Shift+35 exec "~/.config/sway/scripts/new_split_window.sh horizontal"
bindcode $mod+Shift+51 exec "~/.config/sway/scripts/new_split_window.sh tabbed"

# Cycle through window
bindsym $mod+Tab       workspace next
bindsym $mod+Shift+Tab workspace prev

# Change focused window
bindsym $mod+$left  focus left
bindsym $mod+$down  focus down
bindsym $mod+$up    focus up
bindsym $mod+$right focus right
bindsym $mod+Left   focus left
bindsym $mod+Down   focus down
bindsym $mod+Up     focus up
bindsym $mod+Right  focus right

# Move focused window
bindsym $mod+Shift+$left  move left
bindsym $mod+Shift+$down  move down
bindsym $mod+Shift+$up    move up
bindsym $mod+Shift+$right move right
bindsym $mod+Shift+Left   move left
bindsym $mod+Shift+Down   move down
bindsym $mod+Shift+Up     move up
bindsym $mod+Shift+Right  move right

# Resize focused window
bindsym $mod+Ctrl+$left  resize shrink width  10 px or 10 ppt
bindsym $mod+Ctrl+$down  resize shrink height 10 px or 10 ppt
bindsym $mod+Ctrl+$up    resize grow   height 10 px or 10 ppt
bindsym $mod+Ctrl+$right resize grow   width  10 px or 10 ppt
bindsym $mod+Ctrl+Left   resize shrink width  10 px or 10 ppt
bindsym $mod+Ctrl+Down   resize shrink height 10 px or 10 ppt
bindsym $mod+Ctrl+Up     resize grow   height 10 px or 10 ppt
bindsym $mod+Ctrl+Right  resize grow   width  10 px or 10 ppt

# Resizing
mode "resize" {
    bindsym $left  resize shrink width  10 px or 10 ppt
    bindsym $down  resize shrink height 10 px or 10 ppt
    bindsym $up    resize grow   height 10 px or 10 ppt
    bindsym $right resize grow   width  10 px or 10 ppt
    bindsym Left   resize shrink width  10 px or 10 ppt
    bindsym Down   resize shrink height 10 px or 10 ppt
    bindsym Up     resize grow   height 10 px or 10 ppt
    bindsym Right  resize grow   width  10 px or 10 ppt
    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym $mod+r mode "default"
}
bindsym $mod+r mode "resize"

# Swap
mode "swap" {
    # change focus (without mod)
    bindsym Left  focus left
    bindsym Down  focus down
    bindsym Up    focus up
    bindsym Right focus right

    bindsym Return swap container with mark "swapee"; unmark "swapee"; mode "default";
    bindsym Escape unmark "swapee"; mode "default";
}
bindsym $mod+s mark --add "swapee"; mode "swap"

# Workspaces
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+Shift+1 move container to workspace 1; workspace 1
bindsym $mod+Shift+2 move container to workspace 2; workspace 2
bindsym $mod+Shift+3 move container to workspace 3; workspace 3
bindsym $mod+Shift+4 move container to workspace 4; workspace 4
bindsym $mod+Shift+5 move container to workspace 5; workspace 5
bindsym $mod+Shift+6 move container to workspace 6; workspace 6
bindsym $mod+Shift+7 move container to workspace 7; workspace 7
bindsym $mod+Shift+8 move container to workspace 8; workspace 8
bindsym $mod+Shift+9 move container to workspace 9; workspace 9

# Switch the current container between different layout styles
bindsym $mod+comma  layout tabbed
bindsym $mod+period layout stacking
bindsym $mod+slash  layout toggle split

# Split the current object of your focus
bindsym $mod+Shift+c split toggle
bindsym $mod+Shift+b split h
bindsym $mod+Shift+v split v

# Make the current focus fullscreen
bindsym $mod+f fullscreen toggle

# Stick a window to all workspace
bindsym $mod+Shift+s sticky toggle

# Toggle the current focus between tiling and floating mode
bindsym $mod+space floating toggle

# Swap focus between the tiling area and the floating area
bindsym $mod+Shift+space focus mode_toggle

# Move focus to the parent/child container
bindsym $mod+a focus parent
bindsym $mod+z focus parent

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym $mod+minus scratchpad show

# Move the currently focused window to the scratchpad
bindsym $mod+Shift+minus move scratchpad

# Warpd
bindsym $mod+Mod1+x exec warpd --hint
bindsym $mod+g exec warpd --grid
bindsym $mod+b exec warpd --normal
