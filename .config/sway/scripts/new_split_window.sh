#!/usr/bin/env bash

# Get the process id of the currently selected window
pid=$(swaymsg -t get_tree | grep '"focused": true' -A 40 | grep "pid" | grep -o -E '[0-9]+')
# Get the name of this process
pname=$(ps -p "$pid" -o comm=)

# If the process name matches the $terminal, obtain the CWD and echo the result.
if [ "$pname" == "$TERMINAL" ]; then
    # Get parent process id
    ppid=$(pgrep --newest --parent "$pid")
    # Get the current working directory
    workdir=$(readlink /proc/"$ppid"/cwd)
    # Run
    cmd="$TERMINAL --working-directory $workdir"
else
    cmd="$pname"
fi

case $1 in
    v*) swaymsg "split vertical; exec $cmd" ;;
    h*) swaymsg "split horizontal; exec $cmd" ;;
    t*) swaymsg "split horizontal; layout tabbed; exec $cmd" ;;
esac
