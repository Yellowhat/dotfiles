#!/usr/bin/env bash
# Change screen brightness, the input is in percentage
set -euo pipefail

ROOT=$(echo /sys/class/backlight/*)
CUR=$(cat "$ROOT/brightness")
MAX=$(cat "$ROOT/max_brightness")

NEW=$((CUR + MAX * $1 / 100))

if ((NEW > MAX)); then
    NEW=$MAX
elif ((NEW <= 0)); then
    NEW=0
fi

sudo sh -c "echo $NEW >$ROOT/brightness"
