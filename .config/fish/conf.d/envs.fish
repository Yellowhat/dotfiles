#!/usr/bin/env fish
# Set environment variable

set --global --export EDITOR ~/.local/share/flatpak/exports/bin/io.yellowhat.nvim
set --global --export FILE sf
set --global --export MENU menu
set --global --export TERMINAL foot
set --global --export TERMINAL_FLOATING "$(command -v foot) --log-level=none --app-id=term_floating"
set --global --export QT_QPA_PLATFORM wayland
set --global --export QT_STYLE_OVERRIDE "Adwaita-Dark"
set --global --export GTK_THEME "Adwaita:dark"
set --global --export XDG_CURRENT_DESKTOP sway
set --global --export XDG_SESSION_TYPE wayland

# Browsers
if [ $hostname = "janis" ]
    set --global --export BROWSER "flatpak run io.yellowhat.qutebrowser"
    set --global --export BROWSER_PRIVATE $BROWSER \
        --temp-basedir \
        -s content.private_browsing True \
        --config-py $HOME/.var/app/io.yellowhat.qutebrowser/config/qutebrowser/config.py
    set --global --export BROWSER_2 "flatpak run io.yellowhat.librewolf"
    set --global --export BROWSER_2_PRIVATE "$BROWSER_2 --private-window"
else
    set --global --export BROWSER "flatpak run org.mozilla.firefox -P work"
    set --global --export BROWSER_2 "flatpak run io.yellowhat.librewolf"
    set --global --export BROWSER_PRIVATE "$BROWSER_2 --private-window"
    set --global --export BROWSER_2_PRIVATE "$BROWSER --private-window"
end
