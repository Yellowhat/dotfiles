#!/usr/bin/env fish
# Set prompt

function fish_prompt --description "Write out the prompt"
    set --local _status $status

    # Color the prompt differently when we're root
    set --local suffix_color purple
    set --local suffix ">"
    if functions -q fish_is_root_user; and fish_is_root_user
        set suffix_color red
        set suffix "#"
    end

    # Show if git repository is “dirty”, i.e. has uncommitted changes.
    set --global __fish_git_prompt_showdirtystate 1

    # Color last command exit code
    set --local status_color green
    if test $_status -gt 0
        set status_color red
    end

    # Show duration of the last command in seconds
    if test $CMD_DURATION -gt "1000"
        set --local secs (math --scale=1 $CMD_DURATION / 1000 % 60)
        set --local mins (math --scale=0 $CMD_DURATION / 60000 % 60)
        set --local hours (math --scale=0 $CMD_DURATION / 3600000)
        test $hours -gt 0 && set --append duration $hours"h "
        test $mins -gt 0 && set --append duration $mins"m "
        test $secs -gt 0 && set --append duration $secs"s"
    end

    echo -n -s \
        (set_color blue) (prompt_pwd --dir-length 5) (set_color normal) \
        (set_color cyan) (fish_vcs_prompt) (set_color normal) \
        " " (set_color $status_color) $_status (set_color normal) \
        " " (set_color yellow) $duration (set_color normal) \
        " " (set_color $suffix_color) $suffix (set_color normal) \
        " "
end
