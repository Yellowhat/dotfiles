#!/usr/bin/env fish

function serve --description "Start a web server to download/upload files"

    set --local py "$HOME/.cache/serve.py"

    if not test -e $py
        echo "[INFO] Downloading..."
        curl \
            --location \
            --output "$py" \
            "https://gitlab.com/yellowhat-labs/serve/-/raw/main/serve.py"
    end

    python "$py"
end
