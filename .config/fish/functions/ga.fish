#!/usr/bin/env fish

function ga --description "git add wrapper"  --argument-names "option"
    # Check if this is a git repository
    git status >/dev/null || return

    set --local opts "\
        --ansi
        --exit-0
        --multi
        --nth 2..,..
        --preview=\"git diff --color=always -- {-1} | sed 1,4d\"
        --preview-window up:75%
    "
    git -c color.status=always -c status.relativePaths=true status --short --untracked-files |
        sed -E 's/^(..[^[:space:]]*)[[:space:]]+(.*)$/[\1]  \2/' |
        FZF_DEFAULT_OPTS="$opts" fzf |
        sed 's/^.*]  //' |
        sed 's/.* -> //' |
        sed -e 's/^\\\"//' -e 's/\\\"\$//' |
        tr '\n' '\0' |
        xargs -0 -I% git add %

    echo "[INFO] git status"
    git status --short --untracked-files
    echo ""

    switch $option
        case -f --force
            read --prompt-str "Force push [y/N] " confirm
            switch $confirm
                case 1 y ye yes Y Ye Yes YEs YES
                    git commit --amend --no-edit
                    git stash clear
                    git fsck --unreachable --no-reflogs
                    git reflog expire --expire-unreachable=now --all
                    git gc --aggressive --prune=all
                    git repack
                    git push --force
                case "*"
                    echo "[INFO] Stopped"
                    return
            end
        case "*"
            read --prompt-str "Insert commit message: " message
            git commit --message $message
            git push
    end
end
