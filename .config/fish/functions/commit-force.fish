#!/usr/bin/env fish

function commit-force --description "git: commit and push force helper"
    # Check if this is a git repository
    git status >/dev/null || return

    # Force
    set --local root_path $(git rev-parse --show-toplevel)
    echo "[INFO] Repository size: $(du -sh "$root_path")"
    git add .
    git commit --amend --no-edit
    git stash clear
    git fsck --unreachable --no-reflogs
    git reflog expire --expire-unreachable=now --all
    git gc --aggressive --prune=all
    git repack
    git push --force
    echo "[INFO] Repository size: $(du -sh $root_path)"
end
