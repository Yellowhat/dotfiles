#!/usr/bin/env fish

function upd --description "Update the system"
    if type -q fwupdmgr &>/dev/null
        fwupdmgr refresh --force
        fwupdmgr get-updates
        fwupdmgr update
    end

    if type -q emerge &>/dev/null
        sudo eix-sync
        sudo emerge -1auvDN --quiet @world
        sudo emerge -a --depclean
        sudo eclean-dist
        sudo revdep-rebuild -v
        glsa-check -t all

        sudo emerge -1a @live-rebuild
    else
        sudo dnf clean all
        sudo dnf update
        sudo dnf distro-sync
        sudo dnf autoremove
    end

    flatpak update
    flatpak uninstall --unused

    # neovim
    cd "$HOME/.var/app/io.yellowhat.nvim"
    # Not sure why but some files are created read-only
    find . -type d -exec chmod 755 {} +
    find . -type f -exec chmod 644 {} +
    rm -rf .local cache config/go config/nvim/lazy-lock.json config/npmrc data go
    cd -

    flatpak run io.yellowhat.nvim \
        -c "Lazy! sync" \
        -c quitall

    flatpak run io.yellowhat.nvim \
        --headless \
        -c "LspInstall cssls emmet_ls html lemminx golangci_lint_ls gopls biome eslint jsonls lua_ls marksman prosemd_lsp zk pyre pylsp pyright ruff terraformls tflint yamlls ansiblels bashls dockerls helm_ls typos_lsp rust_analyzer" \
        -c quit

    flatpak run io.yellowhat.nvim \
        --headless \
        -c "TSInstallSync! bash comment dockerfile fish go gotmpl hcl hjson json json5 lua make markdown python regex rust terraform vim yaml" \
        -c quit

    # Qutebrowser
    set --local ad_list "$HOME/.var/app/io.yellowhat.qutebrowser/cache/ad.list"
    mkdir -p "$(dirname $ad_list)"
    curl "https://v.firebog.net/hosts/lists.php?type=all" -o "$ad_list"
    echo "https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list/-/raw/master/For%20hosts%20file/The_Quantum_Ad-List.txt" >>"$ad_list"
end
