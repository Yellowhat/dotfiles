#!/usr/bin/env fish

function git-squash-branch --description "git: squash all commit in a branch" --argument-names "message"
    # Check if this is a git repository
    git status >/dev/null || return

    set --local branch_default $(git config --get init.defaultBranch || echo main)
    set --local branch $(git branch --show-current)

    if [ "$branch_default" = "$branch" ]
        echo "[ERROR] Run on a branch, not $branch_default"
        return
    end

    # Check for the commit message
    if not test "$message"
        set message "Initial commit"
    end
    echo "[INFO] Using message: $message"

    # Add
    git reset $(git merge-base main "$branch")
    git add --all
    git commit -m "$message"

    # Accept?
    read --prompt-str "Do you want to push? [y/N] " confirm
    switch $confirm
        case 1 y ye yes Y Ye Yes YEs YES
            echo "[INFO] Continue"
        case "*"
            echo "[INFO] Stopped"
            return
    end

    # Push
    git push --force
end
