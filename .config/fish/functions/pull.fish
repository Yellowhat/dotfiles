#!/usr/bin/env fish

function pull --description "Pull all git repository"
    for i in ~/git/**/.git
        echo $i
        pushd $i/../
        git fetch --prune
        git pull --all
        popd
    end
end
