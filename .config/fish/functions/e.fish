#!/usr/bin/env fish

function e --description "podman exec selector"
    set --local container_id $(podman ps --format "{{.ID}}\t{{.Names}}\t{{.Image}}\t{{.Status}}" | fzf --select-1 | awk '{print $1}')
    echo "[INFO] Container ID: $container_id"

    if podman exec -it "$container_id" bash -c "ls"
        set shell bash
    else if podman exec -it "$container_id" ash -c "ls"
        set shell ash
    else
        set shell sh
    end

    echo "[INFO] Using $shell"
    podman exec -it "$container_id" "$shell"
end
