#!/usr/bin/env fish

function git-from-remote --description "git: force synchronise local repository to remote"
    # Check if this is a git repository
    git status >/dev/null || return

    # Reset
    set --local branch $(git branch --show-current)
    git fetch origin
    git reset --hard "origin/$branch"
    git clean --force
end
