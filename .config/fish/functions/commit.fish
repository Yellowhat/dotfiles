#!/usr/bin/env fish

function commit --description "git: commit and push force helper" --argument-names "message"
    # Check if this is a git repository
    git status >/dev/null || return

    # Check for the commit message
    if not test "$message"
        echo "[ERROR] Commit message missing"
        return
    end

    echo "[INFO] git pull"
    git pull

    # Preview changes
    gd

    # Accept?
    echo "[INFO] Commit message: $message"
    read --prompt-str "Do you want to push? [y/N] " confirm
    switch $confirm
        case 1 y ye yes Y Ye Yes YEs YES
            echo "[INFO] Continue"
        case "*"
            echo "[INFO] Stopped"
            return
    end

    # Commit and push
    git add "$(git rev-parse --show-toplevel)"
    git commit --message "$message"
    git push --all
end
