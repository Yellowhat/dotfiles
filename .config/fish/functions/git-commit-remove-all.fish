#!/usr/bin/env fish

function git-commit-remove-all --description "git: remove all commit history" --argument-names "message"
    # Check if this is a git repository
    git status >/dev/null || return

    # Check for the commit message
    if not test "$message"
        set message "Initial commit"
    end
    echo "[INFO] Using message: $message"

    # Reset
    set --local config $(cat .git/config | string collect)
    set --local branch $(git branch --show-current)
    rm -rf .git

    # Add
    git init --initial-branch "$branch"
    git add .
    git commit --message "$message"
    echo "$config" >.git/config

    # Accept?
    read --prompt-str "Do you want to push? [y/N] " confirm
    switch $confirm
        case 1 y ye yes Y Ye Yes YEs YES
            echo "[INFO] Continue"
        case "*"
            echo "[INFO] Stopped"
            return
    end

    # Push
    git push --set-upstream --force origin "$branch"
end
