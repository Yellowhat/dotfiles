#!/usr/bin/env fish

function purge_old_kernels --description "Remove old kernel version"
    for ver in $(/bin/ls -v /lib/modules | head -n -1)
        set -x
        echo "Removing $ver"
        sudo rm -rf /lib/modules/"$ver"
        sudo rm -rf /boot/*-"$ver" /boot/*-"$ver".img
        sudo rm -rf /usr/src/linux-"$ver"
        set +x
    end
    echo ""

    echo "Regenerate grub"
    sudo grub-mkconfig -o /boot/grub/grub.cfg
end
