#!/usr/bin/env fish

function fcd --description "Go to directory using fzf"
    set --local dir $(find . -type d -not -path "." | fzf --preview "tree -C {} | head -100")

    if test -d $dir
        cd $dir
    end
end
