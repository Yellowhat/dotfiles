#!/usr/bin/env fish

function temp --description "Print sensors data for your hardware"
    cat /sys/class/thermal/thermal_zone*/type /sys/class/thermal/thermal_zone*/temp
    # paste <(cat /sys/class/thermal/thermal_zone*/type) <(cat /sys/class/thermal/thermal_zone*/temp) |
    #     column -s $'\t' -t |
    #     sed 's/\(.\)..$/.\1°C/'
end
