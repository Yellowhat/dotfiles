#!/usr/bin/env fish

function fedit --description "Search a file using fzf and edit it"
    set --local file $(fzf --preview "cat {}")

    if test -e $file
        $EDITOR "$file"
    end
end
