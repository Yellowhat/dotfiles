#!/usr/bin/env fish

function fgrp --description "Search a file by its content using fzf and edit it"
    set --local file $(
        fzf \
            --ansi \
            --phony \
            --bind "change:reload:grep --ignore-case --files-with-matches --recursive {q}" \
            --preview "grep --color=always --ignore-case --context 10 {q} {}" \
            --query ""
    )

    if test -e $file
        $EDITOR "$file"
    end
end
