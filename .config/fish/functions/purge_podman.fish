#!/usr/bin/env fish

function purge_podman --description "Purge podman"
    podman ps --external --quiet | xargs --no-run-if-empty podman rm --force
    podman system prune --all --force
    podman image rm --all
    podman system reset --force
    rm -rf "$HOME/.local/share/containers"

    sudo podman ps --external --quiet | xargs --no-run-if-empty sudo podman rm --force
    sudo podman system prune --all --force
    sudo podman image rm --all
    sudo podman system reset --force
    sudo rm -rf /var/lib/containers/*
end
