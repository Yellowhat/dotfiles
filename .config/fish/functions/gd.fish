#!/usr/bin/env fish

set --export FZF_DEFAULT_OPTS "--ansi --preview-window up:75%"

function usage --description "Show usage for gd"
    echo "
Usage: $(status current-command) OPTION [ARG]

Diff:
b | branches        logs for each branch
h | heads           changes for each commit
r | remotes         logs for each remote origin
s | stashes         show stashes
t | tags            changes for each tag
f | files           diff between local and remote (default)
-h | --help | help  help
"
end

function gd --description "git diff wrapper" --argument-names "option"
    # Check if this is a git repository
    git status >/dev/null || return

    switch $option
        case "b*"
            echo "[INFO] Diff branches"
            git branch --all --color=always | grep -v '/HEAD\s' | sort |
                fzf \
                    --preview 'git log --oneline --graph --date=short --color=always --pretty="format:%C(auto)%cd %h%d %s" $(sed s/^..// <<< {} | cut -d" " -f1)'
        case "h*"
            echo "[INFO] Diff heads"
            git log --date=short --format="%C(green)%C(bold)%cd %C(auto)%h%d %s (%an)" --graph --color=always |
                fzf \
                    --no-sort \
                    --reverse \
                    --bind 'ctrl-s:toggle-sort' \
                    --header 'Press CTRL-S to toggle sort' \
                    --preview 'grep -o "[a-f0-9]\{7,\}" <<< {} | xargs git show --color=always'
        case "r*"
            echo "[INFO] Diff remotes"
            git remote -v | awk '{print $1 "\t" $2}' | uniq |
                fzf \
                    --tac \
                    --preview 'git log --oneline --graph --date=short --pretty="format:%C(auto)%cd %h%d %s" {1}'
        case "s*"
            echo "[INFO] Diff stashes"
            git stash list |
                fzf \
                    --reverse \
                    -d: \
                    --preview 'git show --color=always {1}'
        case "t*"
            echo "[INFO] Diff tags"
            git tag --sort -version:refname |
                fzf \
                    --preview 'git show --color=always {}'
        case -h --help help
            usage
        case "*"
            echo "[INFO] Diff files"
            git status --short |
                fzf \
                    --preview '
                    set --local out $(git diff --color=always -- {-1} | sed 1,4d | string collect)
                    if [ -n "$out" ]
                        echo "$out"
                    else
                        cat {-1}
                    end
                '
            git --no-pager diff --stat --color
    end
end
