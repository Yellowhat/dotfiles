#!/usr/bin/env fish

function gl --description "git log wrapper"
    # Check if this is a git repository
    git status >/dev/null || return

    set --local cmd "echo {} | awk '{print \$2}' | xargs -I% git show --color=always %"
    set --local opts "\
        --ansi
        --preview-window up:75%
        --reverse
        --no-sort
        --tiebreak=index
        --preview=\"$cmd\"
    "
    set --local format '%C(auto)%h%d %s %C(black)%C(bold)%cr%Creset'
    git log --graph --color=always --format="$format" | FZF_DEFAULT_OPTS="$opts" fzf
end
