#!/usr/bin/env fish

function gg --description "git cheatsheet"
    set --local cmd '
git checkout b1 | change current branch
git checkout b2 file1 file2 | integrate files from other branch
git diff @{upstream} | compare local and remote
git diff b1..b2 | compare branches b1 and b2
git branch --delete b1 | delete b1 branch
git checkout -b b1 && git push origin b1 | create a new branch
git commit --amend -m "<msg>" && git push --force | change commit message
'
    commandline $(echo $cmd | fzf --no-sort --tac | cut -d "|" -f 1)
end
