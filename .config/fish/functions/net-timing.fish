#!/usr/bin/env fish

function net-timing --description "Measuring network performance" --argument-names "url"

    curl \
        --location \
        --write-out "
    %{time_namelookup} # namelookup: start to name resolving completed
    %{time_connect} # connect: start to TCP connect to the remote host (or proxy) completed
    %{time_appconnect} # appconnect: start to SSL/SSH/etc connect/handshake to the remote host completed
    %{time_pretransfer} # pretransfer: start to file transfer was just about to begin (includes all pre-transfer commands and negotiations that are specific to the particular protocol(s) involved)
    %{time_redirect} # redirect: name lookup, connect, pretransfer and transfer before the final transaction was started
    %{time_starttransfer} # starttransfer: start to first byte was just about to be transferred
    %{time_total} # total: full operation lasted
    " \
        $url
end
