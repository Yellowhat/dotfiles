#!/usr/bin/env fish

function git-commit-remove --description "git: remove up to a commit"
    # Check if this is a git repository
    git status >/dev/null || return

    # Reset
    set --local commit $(
        git log --graph --color --format="%C(auto)%h%d %s | %C(bold)%cr" |
            fzf --ansi --reverse |
            awk '{print $2}'
    )
    git reset --hard "$commit"

    # Accept?
    read --prompt-str "Do you want to push? [y/N] " confirm
    switch $confirm
        case 1 y ye yes Y Ye Yes YEs YES
            echo "[INFO] Continue"
        case "*"
            echo "[INFO] Stopped"
            return
    end

    # Commit and push
    git push --force
end
