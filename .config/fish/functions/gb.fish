#!/usr/bin/env fish

function gb --description "git change branch"
    # Check if this is a git repository
    git status >/dev/null || return

    set --local branch $(
        git branch --color=always --all --sort=-committerdate |
            grep -v HEAD |
            sed "s/.* //" |
            fzf \
                --ansi \
                --no-multi \
                --preview 'git log -n 50 --color=always --date=short --pretty="format:%C(auto)%cd %h%d %s" {}'
    )

    # If branch name starts with 'remotes/' then it is a remote branch.
    if string match "remotes/*" "$branch"
        git switch (string replace "remotes/origin/" "" "$branch")
    else
        git checkout "$branch"
    end
end
