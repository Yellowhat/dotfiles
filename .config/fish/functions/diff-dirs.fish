#!/usr/bin/env fish

function diff-dirs --description "diff 2 directories" --argument-names "dir0" "dir1"
    # Check inputs
    if not test $dir0; and not test $dir1
        echo "[ERROR] Provide 2 directories"
        return
    end

    # Show summary
    diff --recursive --brief $dir0 $dir1
    read --prompt-str "Press any key to continue" confirm

    # cycle over each file
    for line in $(diff --recursive --brief $dir0 $dir1 | grep -i files);
        set --local compare (string split " " -f2,4 -- $line)
        echo " # $compare"
        diff --unified $compare | less
    end
end
