# ThinkPad W541

## General Setup

[ ] Compile also drivers which will not load
[x] Compile the kernel with warnings as errors
[x] Automatically append version information to the version string
[x] Kernel compression mode (Gzip)
[x] System V IPC
[x] POSIX Message Queues
[ ] General notification queue
[x] Enable process_vm_readv/writev syscalls
[ ] uselib syscall
[x] Auditing support
[x] IRQ subsystem:
    [ ] Expose irq internals in debugfs
[x] Timers subsystem:
    [x] Timer tick handling (Idle dynticks system (tickless idle))
    [x] Old Idle dynticks config
    [x] High Resolution Timer Support
    [125] Clocksource watchdog maximum allowable skew (in   s)
[x] BPF subsystem:
    [x] Enable bpf() system call
    [ ] Enable BPF Just In Time compiler
    [x] Disable unprivileged BPF by default
    [ ] Preload BPF file system with kernel specific program and map iterators
[x] Preemption Model: Preemptible Kernel (Desktop)
[x] Core Scheduling for SMT
[x] CPU/Task time and stats accounting:
    [x] Cputime accounting (Simple tick based cputime accounting)
    [ ] Fine granularity task level IRQ time accounting
    [*] BSD Process Accounting
    [ ]   BSD Process Accounting version 3 file format
    [*] Export task/process statistics through netlink
    [*]   Enable per-task delay accounting
    [*]   Enable extended accounting over taskstats
    [*]     Enable per-task storage I/O accounting
    [ ] Pressure stall information tracking
[x] CPU isolation
[ ] Kernel .config support
[ ] Enable kernel headers through /sys/kernel/kheaders.tar.xz
[x] Scheduler features:
    [ ] Enable utilization clamping for RT/FAIR tasks
[ ] Memory placement aware NUMA scheduler
[x] Control Group support
    [ ] Favor dynamic modification latency reduction by default
    [x] Memory controller
    [x] IO controller
    [x] CPU controller
    [x] PIDs controller
    [ ] RDMA controller
    [x] Freezer controller
    [ ] HugeTLB controller
    [x] Cpuset controller
    [x] Include legacy `/proc/<pid>/cpuset` file
    [x] Device controller
    [x] Simple CPU accounting controller
    [x] Perf controller
    [x] Support for eBPF programs attached to cgroups
    [x] Misc resource controller
    [x] Debug controller
[x] Namespaces support
    [x] UTS namespace
    [x] TIME namespace
    [x] IPC namespace
    [x] User namespace
    [x] PID namespace
    [x] Network namespace
[x] Checkpoint/restore support
[x] Automatic process group scheduling
[x] Initial RAM filesystem and RAM disk (initramfs/initrd) support
[x] Support initial ramdisk/ramfs compressed using gzip
[x] Support initial ramdisk/ramfs compressed using bzip2
[x] Support initial ramdisk/ramfs compressed using LZMA
[x] Support initial ramdisk/ramfs compressed using XZ
[x] Support initial ramdisk/ramfs compressed using LZ0
[x] Support initial ramdisk/ramfs compressed using LZ4
[ ] Boot config support
[x] Preserve cpio archive mtimes in initramfs
[x] Compiler optimization level: Optimize for performance (-O2)
[x] Configure standard kernel features (expert users)
    [x] sgetmask/ssetmask syscalls support
    [x] Sysfs syscall support
[ ] Profiling support
[x] Kexec and crash features
    [ ] kexec system call
    [ ] kexec file based system call
    [ ] kernel crash dumps

## [x] 64-bit kernel

## Processor Type and Features

[x] Symmetric multi-processing support
[x] Enable MPS table
[ ] x86 CPU resource control support
[ ] Support for extended (non-PC) x86 platforms
[x] Intel Low Power Subsystem Support
[ ] AMD ACPI2Platform devices support
[x] Intel SoC IOSF Sideband support for SoC platforms
[ ] Enable IOSF sideband access through debugfs
[x] Single-depth WCHAN output
[ ] Linux guest support
[x] Processor family: Core 2/newer Xeon
[x] Supported processor vendors:
     [x] Support Intel processors (only)
[x] Enable DMI scanning
[ ] Old AMD GART IOMMU support
[ ] Enable Maximum number of SMP Processors and NUMA Nodes
(16) Maximum number of CPUs
[x] Cluster scheduler support
[x] Multi-core scheduler support
[x] CPU core priorities scheduler support
[ ] Reroute for broken boot IRQs
[x] Machine Check / overheating reporting
[ ] Support for deprecated /dev/mcelog character device
[x] Intel MCE features
[x] AMD MCE features
[ ] Machine check injector support
[x] Performance monitoring:
    [x] Intel uncore performance events
    [x] Intel/AMD rapl performance events
    [x] Intel cstate performance events
    [ ] AMD Processor Power Reporting Mechanism
    [ ] AMD Uncore performance events
    [ ] AMD Zen3 Branch Sampling support
[ ] Enable support for 16-bit segments
[x] Enable vsyscall emulation
[x] IOPERM and IOPL Emulation
[x] /dev/cpu/*/msr - Model-specific register support
[x] /dev/cpu/*/cpuid - CPU information support
[ ] Enable 5-level page tables support
[ ] Enable statistic for Change Page Attribute
[ ] AMD Secure Memory Encryption (SME) support
[x] Numa Memory Allocation and Scheduler Support
[ ] Old style AMD Opteron NUMA detection
[x] ACPI NUMA detection
[ ] NUMA emulation
(6) Maximum NUMA Nodes (as a power of 2)
[ ] Support non-standard NVDIMMs and ADR protected memory
[x] Check for low memory corruption
[x] Set the default setting of memory_corruption_check
[x] MTRR (Memory Type Range Register) support
[ ] MTRR cleanup support
[x] x86 PAT support
[x] User Mode Instruction Prevention
[x] Indirect Branch Tracking
[x] Memory Protection Keys
[x] TSX enable mode (off)
[ ] X86 userspace shadow stack
[x] EFI runtime service support
[x] EFI stub support
[x] EFI handover protocol (DEPRECATED)
[x] EFI mixed-mode support
[ ] Enable EFI fake memory map
[x] Export EFI runtime maps to sysf
[x] Timer frequency: 1000 Hz
[x] Randomize the address of the kernel image (KASLR)
[x] Randomize the kernel memory sections
[ ] Linear Address Masking support
[ ] Disable the 32-bit vDSO (needed for glibc 2.3.3)
[x] vsyscall table for legacy applications (Emulate execution only)
[ ] Built-in kernel command line
[*] Enable the LDT (local descriptor table) (NEW)
[ ] Enforce strict size checking for sigaltstack

## [ ] Mitigations for speculative execution vulnerabilities

## Power Management Options (ACPI, APM)

[ ] Suspend to RAM and standby
[ ] Hibernation (aka 'suspend to disk')
[x] Device power management core functionality
[ ] Power Management Debug Support
[ ] Enable workqueue power-efficient mode by default
[x] Energy Model for CPUs
[x] ACPI (Advanced Configuration and Power Interface) Support
    [ ] AML debugger interface
    [ ] ACPI Serial Port Console Redirection Support
    [ ] ACPI Firmware Performance Data Table (FPDT) support
    [x] Allow supported ACPI revision to be overridden
    [ ] EC read/write access through /sys/kernel/debug/ec
    [x] AC Adapter
    [x] Battery
    [x] Button
    [x] Video
    [x] Fan
    [x] Dock
    [x] Processor
    [x] Processor Aggregator
    [x] Thermal Zone
    [x] Allow upgrading ACPI tables via initrd
    [ ] Debug Statements
    [ ] PCI slot detection driver
    [x] Container and Module Devices
    [ ] Smart Battery System
    [ ] Hardware Error Device
    [ ] Allow ACPI methods to be inserted/replaced at run time
    [ ] Boottime Graphics Resource Table support
    [ ] Hardware-reduced ACPI support only
    [ ] ACPI NVDIMM Firmware Interface Table (NFIT)
    [x] NUMA support
    [ ] ACPI Heterogeneous Memory Attribute Table Support
    [ ] ACPI Platform Error Interface
    [ ] Intel DPTF (Dynamic Platform and Thermal Framework) Support
    [ ] ACPI configfs support
    [ ] ACPI Platform Firmware Runtime Update and Telemetry
    [*] ACPI PCC Address Space
    [ ] ACPI FFH Address Space
    [ ] PMIC (Power Management Integrated Circuit) operation region support
    [*] Platform Runtime Mechanism Support
[x] Power Management Timer Support
[x] CPU Frequency scaling
    [x] CPU Frequency scaling
    [x] CPU frequency translation statistics
    [x] Default CPUFreq governor (schedutil)
    [x] 'performance' governor
    [x] 'powersave' governor
    [x] 'userspace’ governor for userspace frequency scaling
    [x] 'ondemand' cpufreq policy governor
    [ ] 'conservative' cpufreq governor
    [x] 'schedutil' cpufreq policy governor
    [x] Intel P state control
    [ ] Processor Clocking Control interface driver
    [ ] AMD Processor P-State driver
    [ ] selftest for AMD Processor P-State driver
    [x] ACPI Processor P-States driver: on
    [ ] AMD Opteron/Athlon64 PowerNow!
    [ ] AMD frequency sensitivity feedback powersave bias
    [ ] Intel Enhanced SpeedStep (deprecated)
    [ ] Intel Pentium 4 clock modulation
[x] CPU idle
    [x] CPU idle PM support
    [ ] Ladder governor (for periodic timer tick)
    [x] Menu governor (for tickless system)
    [ ] Timer events oriented (TEO) governor (for tickless systems)
[x] Cpuidle Driver for Intel Processors

## Bus Option (PCI, etc)

[x] Support mmconfig PCI config space access
[ ] Read CNB20LE Host Bridge Windows
[ ] ISA bus support on modern systems
[x] ISA-style DMA support

## Binary Emulations

[ ] IA32 Emulation
[ ] x32 ABI for 64-bit mode

## [x] Virtualization

[x] Kernel-based Virtual Machine (KVM) support
[x] Compile KVM with -Werror
[x] KVM for Intel processors support
[ ] KVM for AMD processors support
[ ] Support for Microsoft Hyper-V emulation
[ ] Support for Xen hypercall interface
[ ] Prove KVM MMU correctness

## General architecture-dependent options

[ ] Kprobes
[x] Optimize very unlikely/likely branches
[ ] Static key selftest
[ ] Static call selftest
[x] Enable seccomp to safely execute untrusted bytecode
[ ] Show seccomp filter cache status in /proc/pid/seccomp_cache
[x] Stack Protector buffer overflow detection
[x] Strong Stack Protector
[x] Link Time Optimization (LTO) (None)
(28) Number of bits to use for ASLR of mmap base address
[x] Provide system calls for 32-bit time_t
[x] Use a virtually-mapped stack
[x] Support for randomizing kernel stack offset on syscall entry
[ ] Default state of kernel stack offset randomization
[ ] Locking event counts collection
[ ] GCOV-based kernel profiling
[ ] GCC plugins

## [x] Enable loadble module support

[ ] Forced module loading
[x] Module unloading
[x] Forced module unloading
[ ] Module version support
[ ] Source checksum for all modules
[ ] Module signature verification
[ ] Module compression mode (None)
[ ] Allow loading of modules with missing namespace imports
[ ] Trim unused exported kernel symbols

## [x] Enable the Block Layer

[x] Block layer SG support v4
[ ] Block layer SG support v4 helper lib
[ ] Block layer data integrity support
[x] Allow writing to mounted block devices
[ ] Zoned block device support
[ ] Block layer bio throttling support
[ ] Enable support for block devices writeback throttling
[x] Enable support for latency based cgroup IO protection
[x] Enable support for cost model based cgroup IO controller
[x] Cgroup I/O controller for assigning on I/O priority class
[x] Block layer debugging information in debugfs
[ ] Logic for interfacing with Opal enabled SEDs
[ ] Enable inline encryption support in block layer
[x] Partition Types:
    [x] Advanced partition selection
    [x] PC BIOS (MS-DOS partition tables) support
    [x] EFI GUID Partition support
[x] IO Schedulers:
    [x] MQ deadline I/O scheduler
    [x] Kyber I/O scheduler
    [ ] BFQ I/O schedule

## Executable file formats

[x] Kernel support for ELF binaries
[x] Write ELF core dumps with partial segments
[x] Kernel support for scripts starting with #!
[x] Kernel support for MISC binaries
[x] Enable core dump support

## Memory Management options

[ ] Support for paging of anonymous memory (swap)
[x] Slab allocator options
    [ ] Enable per cpu partial caches
[x] Sparse Memory virtual memmap
[ ] Memory hotplug
[x] Allow for memory compaction
[ ] Free page reporting
[x] Page migration
[ ] Enable KSM for page merging
[ ] Enable recovery from hardware memory errors
[x] Transparent Hugepage Support
    [x] Transparent Hugepage Support sysfs defaults (always)
[ ] Contiguous Memory Allocator
[ ] Defer initialisation of struct pages to kthreads
[ ] Enable idle page tracking
[x] Support DMA zone
[x] Support DMA32 zone
[x] Enable VM event counters for /proc/vmstat
[x] Collect percpu memory statistics

## Networking Support

[x] Networking options
    [x] TCP/IP networking
    [x] IP: Foo (IP protocols) over UDP
    [x] The IPv6 protocol
       [x] IPv6: Multiple Routing Tables
    [x] Network packet filtering framework (Netfilter)
       [x] Advanced netfilter configuration
       [x] Core Netfilter Configuration
            [x] Netfilter connection tracking support
            [x] Network Address Translation support
            [x] Netfilter nf_tables support
            [x] Netfilter nf_tables conntrack module
            [x] Netfilter nf_tables log module
            [x] Netfilter nf_tables limit module
            [x] Netfilter nf_tables masquerade support
            [x] Netfilter Xtables support
            [x] ctmark target and match support
            [x] "addrtype" address type match support
            [x] "comment" match support
            [x] "hashlimit" match support
            [x] "mark" match support
            [x] "multiport" Multiple port match support
       [x] IP virtual server support
       [x] IP: Netfilter Configuration:
            [M] MASQUERADE target support
            [M] NETMAP target support
            [M] REDIRECT target support
            [M] raw table support
       [x] IPv6: Netfilter Configuration:
            [M] raw table support
    [x] 802.1d Ethernet Bridging
    [x] IGMP/MLD snooping
    [ ] VLAN filtering
    [ ] MRP protocol
    [ ] CFM protocol
    [x] 802.1Q/802.1ad VLAN Support
[ ] Amateur radio support
[ ] CAN bus subsystem support
[x] Bluetooth subsystem support
    [x] Bluetooth Classic (BR/EDR) features
    [x] RFCOMM protocol support
    [x] RFCOMM TTY support
    [x] BNEP protocol support
    [x] Multicast filter support
    [x] Protocol filter support
    [x] HIDP protocol support
    [x] Bluetooth High Speed (HS) features
    [x] Bluetooth Low Energy (LE) features
    [x] Enable LED triggers
    [ ] Enable Microsoft extensions
    [ ] Enable Android Open Source Project extensions
    [ ] Export Bluetooth internals in debugfs
    [ ] Bluetooth self testing support
    [ ] Enable runtime option for debugging statements
    [x] Bluetooth device dri
        [m] HCI USB driver
[x] Wireless
    [m] cfg80211 – wireless configuration API
        [ ] enable powersave by default
        [x] support CRDA
        [x] cfg80211 wireless extensions compatibility
    [m] Generic IEEE 802.11 Networking Stack:
    [x] Minstrel
    [x] Default rate control algorithm: Minstrel
    [x] Enable LED triggers
[x] RF switch subsystem support
[ ] Plan 9 Resource Sharing Support (9P2000)
[ ] NFC subsystem support

## Device Drivers

[ ] EISA support
[x] PCI support
    [x] PCI Express Port Bus support
    [x] PCI Express Advanced Error Reporting support
    [x] PCI Express ASPM control
    [ ] Support for PCI Hotplug
[ ] PCCard support
[ ] RapidIO support
[x] Generic Driver Options
    [ ] Support for uevent helper
    [x] Maintain a devtmpfs filesystem to mount at /dev
    [x] Automount devtmpfs at /dev, after the kernel mounted the rootfs
    [x] Select only drivers that don’t need compile-time external firmware
    [x] Disable drivers features which enable custom firmware building
    [x] Firmware loader:
        [x] Firmware loading facility
[x] Connector - unified userspace <-> kernelspace linker
[ ] Parallel port support
[x] Serial ATA and Parallel ATA drivers:
    [x] Verbose ATA error reporting
    [x] "libata.force=" kernel parameter support
    [x] ATA ACPI Support
    [ ] SATA Zero Power Optical Disc Drive (ZPODD) support
    [x] SATA Port Multiplier support
    [x] AHCI SATA Support
    [x] ATA SFF support
    [x] ATA BMDMA support
    [x] Intel ESB, ICH, PIIX3, PIIX4, PATA/SATA support
[ ] Multiple devices driver support
[ ] Fusion MPT device support
[ ] IEEE 1394 (FireWire) support:
    [ ] FireWire driver stack
[ ] Macintosh device drivers
[x] Network device support
    [x] Network core driver support
    [x] WireGuard secure network tunnel
    [ ] Network console logging support
    [x] Universal TUN/TAP device driver support
    [x] Virtual ethernet pair device
    [x] Ethernet driver support:
        [x] Intel devices
        [x] Intel(R) PRO/1000 PCI-Express Ethernet support
        [x] Support HW cross-timestamp on PCH devices
    [ ] FDDI driver support
    [m] PHY Device support and infrastructure
    [m] USB Network Adapters
        [m] Multi-purpose USB Networking Framework
        [m] Host for RNDIS and ActiveSync devices
    [x] Wireless LAN
        [x] Intel devices
        [m] Intel Wireless WiFi Next Gen AGN – Wireless-N/Advanced-N/Ultimate-N
        [m] Intel Wireless WiFi DVM Firmware support
        [m] Intel Wireless WiFi MVM Firmware support
[x] Input device support
    [x] Export input device LEDs in sysfs
    [x] Event interface
    [x] Keyboards
        [x] AT keyboard
    [x] Mice
        [x] PS/2 mouse
    [ ] Joysticks/Gamepads
    [ ] Tablets
    [ ] Touchscreens
    [x] Miscellaneous devices
        [x] PC Speaker support
        [x] User level driver support
[x] Character devices
    [x] Enable TTY
    [x] Unix98 PTY support
[x] I2C support
    [x] ACPI I2C Operation region support
    [x] Enable compatibility bits for old user-space
    [x] I2C device interface
    [ ] I2C bus multiplexing support
    [x] Autoselect pertinent helper modules
    - I2C Hardware Bus support:
        [x] Intel 82801 (ICH/PCH)
    [ ] I2C/SMBus Test Stub
    [ ] I2C slave support
    [ ] I2C Core debugging messages
    [ ] I2C Algorithm debugging messages
    [ ] I2C Bus debugging messages
[x] Hardware Monitoring support
    [x] Intel Core/Core2/Atom temperature sensor
    [x] ACPI 4.0 power meter
[x] Thermal drivers
    [x] Expose thermal sensors as hwmon device
    [x] Default Thermal governor (step_wise)
    [ ] Thermal emulation mode support
    [x] Intel thermal drivers:
        [x] Intel PowerClamp idle injection driver
        [x] X86 package temperature thermal driver
        [x] ACPI INT340X thermal drivers:
            [x] ACPI INT340X thermal drivers
            [x] ACPI INT3406 display thermal drivers
[x] Watchdog Timer Support
    [x] Intel TCO Timer/Watchdog
    [x] Intel TCO Timer/Watchdog Specific Vendor Support
[x] Multifunction device drivers
    [x] Intel ICH LPC
    [x] Intel SCH LPC
[x] Multimedia support
    Media core support
        [x] Video4Linux core
        [x] Media Controller API
    Digital TV options:
        [ ] all
    Media drivers
        [x] Media USB Adapters
            [x] USB Video Class (UVC)
            [x] UVC input events device support
        [ ] Radio Adapters
    Media ancillary devices
        [ ] all
[x] Graphics Support:
    [ ] /dev/agpgart (AGP Support)
    [ ] Laptop Hybrid Graphics - GPU switching support
    [x] Direct Rendering Manager (XFree86 4.1.0 and higher DRI support)
    [x] DRM DP AUX Interface
    [x] Enable DisplayPort CEC-Tunneling-over-AUX HDMI support
    [ ] ATI Radeon
    [ ] AMD GPU
    [ ] Nouveau (NVIDIA) cards
    [ ] Nouveau legacy context support
    [5] Maximum debug level
    [3] Default debug level
    [ ] Enable additional MMU debugging
    [ ] Enable additional push buffer debugging
    [x] Support for backlight control
    [x] Intel 8xx/9xx/G3x/G4x/HD Graphics
    [x] Enable capturing GPU state following a hang
    [x] Compress GPU error state
    [x] Always enable userptr support
    [ ] Enable Intel GVT-g graphics virtualization host support
    [ ] DRM driver for VMware Virtual GPU
    [x] Frame buffer devices:
        [x] Support for frame buffer device drivers:
            [x] Disable all
        [x] Enable firmware EDID
        [x] Enable Video Mode Handling Helpers
        [x] Enable Tile Blitting Support
        - Disable rest
    [x] Backlight & LCD device support
        [x] Lowlevel Backlight controls
    [x] Console display driver support:
        [x] Framebuffer Console support
    [x] Boot-up logo
        [ ] Standard black and white Linux logo
        [ ] Standard 16-color Linux logo
        [x] Standard 224-color Linux logo
[x] Sound card support
    [x] Advanced Linux Sound Architecture
        [ ] Enable OSS Emulation
        [x] PCM timer interface
        [x] HR-timer backend support
        [ ] Dynamic device file minor number
        [ ] Support old ALSA API
        [x] Sound Proc FS Support
        [x] Verbose procfs contents
        [ ] Verbose printk
        [ ] Debug
        [ ] Sequencer support
        [ ] Generic sound devices
        [x] PCI sound devices
            [ ] all
        - HD-Audio:
            [x] HD Audio PCI
            [x] Build hwdep interface for HD-audio driver
            [x] Allow dynamic codec reconfiguration
            [x] Support digital beep via input layer
            [x] Build Realtek HD-audio codec support
            [x] Build HDMI/DisplayPort HD-audio codec support
            [x] Enable generic HD-audio codec parser
        [x] USB sound devices
            [m] USB Audio/MIDI drive
        [ ] ALSA for SoC audio support
        [ ] X86 sound devices
[x] HID Support
    [x] Battery level reporting for HID devices
    [x] /dev/hidraw raw HID device support
    [x] User-space I/O driver support for HID subsystem
    [x] Generic HID support
    [ ] Special HID drivers (remove all)
    - USB HID support:
        [x] USB HID transport layer
        [x] PID device support
        [x] /dev/hiddev raw HID device support
[x] USB support
    [x] Support for Host-side USB
    [x] USB announce new devices
    [x] Enable USB persist by default
    [x] xHCI HCD (USB 3.0) support
    [x] EHCI HCD (USB 2.0) support
    [x] OHCI HCD (USB 1.1) support
    [ ] USB Printer support
    [x] USB Mass Storage support
[x] MMC/SD card support
    [x] Secure Digital Host Controller Interface support
    [x] SDHCI support on PCI bus
    [x] Command Queue Host Controller Interface Support
[x] LED Support
[ ] InfiniBand support
[x] EDAC (Error Detection And Correction) reporting
    [ ] EDAC legacy sysfs
    [x] Intel e312xx
[x] DMA Engine support
[x] VHOST drivers
    [x] Host kernel accelerator for virtio net
[x] x86 Platform Specific Device Drivers
    [m] ThinkPad ACPI Laptop Extras
        [x] Console audio control ALSA interface
        [ ] Maintainer debug facilities
        [ ] Verbose debug mode
        [ ] Allow control of import LEDs
        [x] Video output control support
        [x] Support NVRAM polling for hot keys
    [x] Intel Intelligent Power Sharing
    [x] Intel Speed Select Technology interface support
    [x] Intel Uncore frequency control driver
[x] IOMMU Hardware Support
    [ ] AMD IOMMU support
    [x] Support for Intel IOMMU using DMA Remapping Devices
    [x] Support for Shared Virtual Memory with Intel IOMMU
    [x] Enable Intel DMA Remapping Devices by default
    [x] Support for Interrupt Remapping
[x] Generic powercap sysfs driver
    [x] Intel RAPL Support via MSR Interface

## File Systems

[ ] Second extended fs support
[ ] Ext3 journalling file system support
[x] The Extended 4 (ext4) filesystem
[x] Use ext4 for ext2 file systems
[x] Ext4 POSIX Access Control Lists
[x] Ext4 Security Labels
[ ] Ext4 debugging support
[ ] JBD2 (ext4) debugging support
[x] XFS filesystem support
[x] Support deprecated V4 (crc=0) format
[ ] XFS Quota support
[x] XFS POSIX ACL support
[ ] XFS Realtime subvolume support
[ ] XFS online metadata check support
[ ] XFS Verbose Warnings
[ ] XFS Debugging support
[x] Inotify support for userspace
[x] Filesystem wide access notification
[x] Kernel automounter support
[x] FUSE (Filesystem in Userspace) support
[x] CD-ROM/DVD Filesyst
    [x] ISO 9660 CDROM file system support
    [x] Microsoft Joliet CDROM extensions
    [x] Transparent decompression extension
    [x] UDF file system support
[x] DOS/FAT/NT Filesystem
    [x] MS-DOS fs support
    [x] VFAT (Windows-95) fs support
    [x] Enable FAT UTF-8 option by default
    [x] exFAT filesystem support
    [x] NTFS file system support
    [x] NTFS write support
[x] Pseudo filesystem
    [x] /proc file system support
    [x] /proc/kcore support
    [x] Sysctl support (/proc/sys)
    [x] Enable /proc page monitoring
    [x] sysfs file system support
    [x] Tmpfs virtual memory file system support (former shm fs)
    [x] Tmpfs POSIX Access Control Lists
    [x] Tmpfs extended attributes
    [x] HugeTLB file system support
    [ ] Userspace-driven configuration filesystem

## Security Options

[x] Diffie-Hellman operations on retained keys

## [x] Cryptographic API

[x] Crypto core or helper:
    [x] Cryptographic algorithm manager
    [x] Userspace cryptographic algorithm configuration
    [x] Parallel crypto engine
[x] Public-key cryptography:
    [x] RSA (Rivest-Shamir-Adleman)
    [x] DH (Diffie-Hellman)
    [x] ECDH (Elliptic Curve Diffie-Hellman)
[x] Block ciphers:
    [x] AES (Advanced Encryption Standard)
[x] CRCs (cyclic redundancy checks)
    [x] CRC32c
    [x] CRC32
[x] Accelerated Cryptographic Algorithms for CPU (x86)
    [x] Public key crypto: Curve25519 (ADX)
    [x] Ciphers: AES, modes: ECB, CBC, CTS, CTR, XTR, XTS, GCM (AES-NI)
    [x] Ciphers: ChaCha20, XChaCha20, XChaCha12 (SSSE3/AVX2/AVX-512VL)
    [x] Hash functions: BLAKE2s (SSSE3/AVX-512)
    [x] Hash functions: Poly1305 (SSE2/AVX2)
    [x] Hash functions: SHA-1 (SSSE3/AVX/AVX2/SHA-NI)
    [x] Hash functions: SHA-224 and SHA-256 (SSSE3/AVX/AVX2/SHA-NI)
    [x] Hash functions: SHA-384 and SHA-512 (SSSE3/AVX/AVX2)

## Library routines

[x] CRC calculation for the T10 Data Integrity Field

## Kernel Hacking

[x] Memory Debugging
    [ ] Enable SLUB debugging support
[x] Kernel Testing and Coverage
    [ ] Runtime Testing
