#!/usr/bin/env bash
# Setup and compile a new kernel
# https://docs.slackware.com/howtos:slackware_admin:building_the_linux_kernel_using_git_repository?do=export_pdf
# https://github.com/gg7/gentoo-kernel-guide
set -euo pipefail

# Select kernel
# eselect kernel list
# eselect kernel set 2
# cd /usr/src/linux

# Delete most generated files
make clean

# Delete the current configuration and all generated files
make mrproper

# Generate default config
make defconfig

# Apply custom config
./scripts/kconfig/merge_config.sh .config config-W541

# Compile
time make -j 5

# Install
make modules_install
make install

# Update grub
grub-mkconfig -o /boot/grub/grub.cfg

# Diff between kernel configs:
# diff <(sort .config) <(sort .config.old) | awk '/^>.*(=|Linux)/ { $1=""; print }'
