#!/usr/bin/env bash
# Benchmark (https://github.com/ThomasKaiser/sbc-bench)
set -euo pipefail

ROOT=/tmp
GL_DIR="${ROOT}/glmark2"
MEM_DIR="${ROOT}/tinymembench/"
MPV_DIR="${ROOT}/mpv"
VTE_DIR="${ROOT}/vtebench"

build_glmark2() {
    echo "[INFO] glmark2 - Build - Start"
    rm -rf "$GL_DIR"
    git clone https://github.com/glmark2/glmark2 --depth 1 "$GL_DIR"
    cd "$GL_DIR"
    meson setup build -Dflavors=wayland-gl,wayland-glesv2 -Ddata-path="$PWD/data/"
    ninja -C build
    echo "[INFO] glmark2 - Build - End"
    echo ""
}

run_glmark2() {
    SECONDS=0
    for exe in glmark2-wayland glmark2-es2-wayland; do
        local log="$GL_DIR/${exe}.log"
        echo "[INFO] $exe - Start"
        "${GL_DIR}/build/src/${exe}" --off-screen --size 1920x1080 | tee "$log"
        version=$(grep -o "glmark2 [0-9]*\.[0-9]*" "$log" | cut -d " " -f 2)
        hw=$(grep "GL_RENDERER:" "$log" | tr -s " " | cut -d " " -f 3-)
        gl=$(grep "GL_VERSION:" "$log" | tr -s " " | cut -d " " -f 3-)
        score=$(grep -o "Score: [0-9]*" "$log" | cut -d " " -f 2)
        echo "${exe},version,${version}" | tee -a "$OUTFILE"
        echo "${exe},GPU,${hw}" | tee -a "$OUTFILE"
        echo "${exe},OpenGL,${gl}" | tee -a "$OUTFILE"
        echo "${exe},score,${score}" | tee -a "$OUTFILE"
        echo "[INFO] $exe - End"
        echo ""
    done
    echo "glmark2,runtime,$((SECONDS / 60)):$((SECONDS % 60))" | tee -a "$OUTFILE"
}

build_mpv() {
    local url="https://www.youtube.com/watch?v=R8m7lw-rTG0"
    rm -rf "$MPV_DIR"
    for format in $(yt-dlp --list-formats "$url" | grep "1080\|1440\|2160\|4320" | cut -d " " -f 1); do
        local folder="${MPV_DIR}/${format}"
        mkdir -p "$folder"
        echo "[INFO] mpv $format - Download - Start"
        yt-dlp \
            --format "$format" \
            --paths "$folder" \
            --output 'video_%(height)sp%(fps)s-%(vbr)dk-%(vcodec)s.%(ext)s' \
            "$url"
        echo "[INFO] mpv $format - Download - End"
        echo ""
    done
}

run_mpv() {
    SECONDS=0
    echo "mpv,version,$(mpv --version | head -1 | cut -d ' ' -f 2)" | tee -a "$OUTFILE"
    local msg="FPS: \${estimated-display-fps}\nDropped: \${frame-drop-count}\n"
    find "$MPV_DIR" -type f -not -name "*.log" -print0 | while IFS= read -r -d '' video; do
        local log="${video}.${governor}.log"
        echo "[INFO] mpv $video - Start"
        mpv \
            --start=10 \
            --end=20 \
            --fs \
            --loop=no \
            --loop-file=no \
            --loop-playlist=no \
            --untimed \
            --opengl-swapinterval=0 \
            --video-sync=display-desync \
            --osd-msg1="$msg" \
            --term-status-msg="$msg" \
            "$video" &>"$log"
        dropped=$(grep -o "Dropped: [0-9]*" "$log" | tail -1 | cut -d " " -f 2)
        fps=$(grep -o "FPS: [0-9]*\.[0-9]*" "$log" | tail -1 | cut -d " " -f 2)
        echo "mpv,${video##*_},${fps}/${dropped}" | tee -a "$OUTFILE"
        echo "[INFO] mpv $video - End"
        echo ""
    done
    echo "mpv,runtime,$((SECONDS / 60)):$((SECONDS % 60))" | tee -a "$OUTFILE"
}

run_openssl() {
    SECONDS=0
    echo "openssl,version,$(openssl version)" | tee -a "$OUTFILE"
    for size in 128 192 256; do
        name="aes-${size}-cbc"
        echo "[INFO] OpenSSL $name - Run - Start"
        ssl_res="${ROOT}/${name}"
        time taskset -c 0 openssl speed -elapsed -evp "$name" | tee "$ssl_res"
        grep -i "$name" -B 1 "$ssl_res" |
            awk -v name="$name" -F '[[:space:]][[:space:]]+' '{ for (i=2;i<=NF;i++) RtoC[i]= (RtoC[i]!=""? RtoC[i] "," $i: $i) } END{for (i in RtoC) print name "," RtoC[i] }' |
            tee -a "$OUTFILE"
        echo "[INFO] OpenSSL $name - Run - End"
        echo ""
    done
    echo "openssl,runtime,$((SECONDS / 60)):$((SECONDS % 60))" | tee -a "$OUTFILE"
}

build_tinymembench() {
    echo "[INFO] tinymembench - Build - Start"
    rm -rf "$MEM_DIR"
    git clone https://github.com/ssvb/tinymembench --depth 1 "$MEM_DIR"
    cd "$MEM_DIR"
    make
    echo "[INFO] tinymembench - Build - End"
    echo ""
}

run_tinymembench() {
    SECONDS=0
    echo "[INFO] tinymembench - Run - Start"
    mem_res="${MEM_DIR}/core0.res"
    time taskset -c 0 "${MEM_DIR}/tinymembench" &>"$mem_res"
    awk '/^ standard mem/ {print "tinymembench," $2 "," $4 " " $5}' <"$mem_res" | tee -a "$OUTFILE"
    for page in "MADV_NOHUGEPAGE" "MADV_HUGEPAGE"; do
        awk -v RS="" "/${page}/" <"$mem_res" |
            awk -v page="$page" 'END {print "tinymembench,srr/drr " $1 " (" page ")," $3 "/" $6 " " $7}' |
            tee -a "$OUTFILE"
    done
    echo "tinymembench,runtime,$((SECONDS / 60)):$((SECONDS % 60))" | tee -a "$OUTFILE"
    echo "[INFO] tinymembench - Run - End"
    echo ""
}

build_vtebench() {
    echo "[INFO] vteench - Build - Start"
    rm -rf "$VTE_DIR"
    git clone https://github.com/alacritty/vtebench/ --depth 1 "$VTE_DIR"
    cd "$VTE_DIR"
    cargo build --release
    echo "[INFO] vtebench - Build - End"
    echo ""
}

run_vtebench() {
    SECONDS=0
    echo "vtebench,terminal,$($TERMINAL --version)" | tee -a "$OUTFILE"
    echo "vtebench,rows cols,$(stty size)" | tee -a "$OUTFILE"
    echo "[INFO] vtebench - Run - Start"
    vte_res="${VTE_DIR}/result.dat"
    cd "$VTE_DIR"
    cargo run --release -- --dat "$vte_res"
    python - <<EOF | tee -a "$OUTFILE"
with open("$vte_res", "r", encoding="UTF-8") as obj:
    lines = [line.split() for line in obj.readlines()]
lines = list(map(list, zip(*lines)))
for line in lines:
    values = [float(v) for v in line if v.isdigit()]
    print(f"vtebench,{line[0]},{sum(values)/len(values):.2f}")
EOF
    echo "vtebench,runtime,$((SECONDS / 60)):$((SECONDS % 60))" | tee -a "$OUTFILE"
    echo "[INFO] vtebench - Run - End"
    echo ""
}

# Build
build_glmark2
build_mpv
build_tinymembench
build_vtebench

# Run
for governor in powersave schedutil performance; do
    OUTFILE="${ROOT}/out-${governor}.res"
    echo "software,kernel,$(uname -r)" | tee "$OUTFILE"
    ~/.local/bin/cpu_governor "$governor"
    run_glmark2
    run_mpv
    run_openssl
    run_tinymembench
    run_vtebench
    echo "[INFO] Results for $governor"
    cat "$OUTFILE"
done
