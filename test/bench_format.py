#!/usr/bin/env python
"""Format benchmark result"""

from collections import defaultdict
from subprocess import run

# Read existing
run("git clone https://gitlab.com/Yellowhat/dotfiles.wiki.git /tmp/wiki", shell=True, check=True)

with open("/tmp/wiki/benchmark.md", encoding="UTF8") as obj:
    lines = obj.read().split("\n\n")

results = defaultdict(dict)
for governor, data in zip(lines[::2], lines[1::2]):
    governor = governor.split()[-1].strip().lower()
    data = data.split("\n")
    headers = [hd.strip() for hd in data[0].split("|")[1:-1]]
    table = defaultdict(dict)
    for line in data[2:-1]:
        row = [r.strip() for r in line.split("|")[1:-1]]
        for i in range(2):
            if not row[i]:
                row[i] = list(table.keys())[-1][i]
        key = tuple(r.strip() for r in row[:2])
        table[key] = dict(zip(headers[2:], [r.strip() for r in row[2:]]))
    results[governor] = table
headers += ["New"]

# Add
for governor in ["powersave", "schedutil", "performance"]:
    table = defaultdict(dict)
    with open(f"/tmp/out-{governor}.res", encoding="UTF8") as obj:
        lines = obj.read().split("\n")[:-1]
    for line in lines:
        row = line.split(",")
        results[governor][(row[0], row[1])].update({headers[-1]: row[-1]})

# Show
with open("results.md", "w", encoding="UTF8") as obj:
    for governor, table in results.items():
        obj.write("# " + governor.capitalize() + "\n")
        obj.write("\n")
        obj.write("| " + " | ".join(headers) + " |\n")
        obj.write("|-" + "-|-".join("-" * 3 for hd in headers) + "-|\n")
        for row_k, row_v in table.items():
            obj.write("| " + " | ".join(row_k) + " | " + " | ".join(row_v.values()) + " |\n")
        obj.write("\n")
