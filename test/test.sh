#!/usr/bin/env bash
# Test suite
set -euo pipefail

# Podman
podman run \
    --interactive \
    --tty \
    --rm \
    quay.io/fedora/fedora \
    echo "Hello World"

podman run \
    --interactive \
    --tty \
    --rm \
    --volume "$PWD":/data \
    --security-opt label=disable \
    quay.io/fedora/fedora \
    ls /data

# https://mozilla.github.io/webrtc-landing/gum_test.html
# - Camera
# - Microphone
# - Screen Capture
