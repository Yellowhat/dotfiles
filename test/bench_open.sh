#!/usr/bin/env bash
# Benchmark: time to open
set -euo pipefail

num_test=3
OUTFILE=~/bench_time.out
export TIMEFORMAT="%3R"

echo "software,kernel,$(uname -r)" | tee -a "$OUTFILE"
echo "software,terminal,$($TERMINAL --version)" | tee -a "$OUTFILE"
echo "software,browser,$($BROWSER --version 2>/dev/null | grep commit)" | tee -a "$OUTFILE"
echo "software,editor,$($EDITOR --version | head -1)" | tee -a "$OUTFILE"

run() {
    local i
    echo "[INFO] $1 - ${governor}" | tee -a "$OUTFILE"
    for i in $(seq "$num_test"); do
        res=$({ time $1 &>/dev/null; } 2>&1)
        echo "$i - $res" | tee -a "$OUTFILE"
    done
}

for governor in powersave schedutil performance; do
    ~/.local/bin/cpu_governor "$governor"
    run "$TERMINAL /bin/true"
    run "$TERMINAL $EDITOR -c :quit"
    run "$TERMINAL $EDITOR -c :quit /tmp/z.py"
    run "$BROWSER_PRIVATE :quit"
    echo "" | tee -a "$OUTFILE"
done
